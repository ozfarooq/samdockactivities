FROM node:12-alpine

WORKDIR /var/www

COPY . .

CMD ["npm", "run", "start:prod"]
