import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadTaskQuery } from '../impl/read-task.query';
import { Task } from '../../models/task.model';
import { TaskRepository } from '../../repositories/task.repository';

@QueryHandler(ReadTaskQuery)
export class ReadTaskQueryHandler implements IQueryHandler<ReadTaskQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: TaskRepository) { }

  async execute(query: ReadTaskQuery): Promise<Task> {
    const { id, meta } = query;
    try {
      return this.repo.read(id, meta);
    } catch (error) {
      this.logger.error(error.message);
      throw error;
    }
  }
}
