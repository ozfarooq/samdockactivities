import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { BrowseTasksQuery } from '../impl/browse-tasks.query';
import { Logger } from '@nestjs/common';
import { TaskRepository } from '../../repositories/task.repository';
import { Task } from '../../models/task.model';
import { PaginationResult } from '../../../shared/bread.respository';

@QueryHandler(BrowseTasksQuery)
export class BrowseTasksQueryHandler implements IQueryHandler<BrowseTasksQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: TaskRepository) { }

  async execute(query: BrowseTasksQuery): Promise<PaginationResult<Task> | Task[]> {
    const { filter, limit, offset, meta } = query;
    try {
      if (!filter) {
        return this.repo.browse(limit, offset, meta);
      }
      if (filter._personID) {
        return this.repo.browseByPersons(filter._personID, limit, offset, meta);
      }
      if (filter._organizationID) {
        return this.repo.browseByOrganizations(filter._organizationID, limit, offset, meta);
      }
    } catch (error) {
      this.logger.error(error.message);
      throw error;
    }
  }
}
