import { BrowseTasksQueryHandler } from './browse-tasks.query-handler';
import { ReadTaskQueryHandler } from './read-tasks.query-handler';

export const QueryHandlers = [
  BrowseTasksQueryHandler,
  ReadTaskQueryHandler,
];
