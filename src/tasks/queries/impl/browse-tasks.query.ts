import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class BrowseTasksQuery implements IQuery {
  constructor(
    public filter: { [key: string]: any },
    public limit: number,
    public offset: number,
    public meta: EventMetaData,
  ) {}
}
