import { IQuery } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ReadTaskQuery implements IQuery {
  constructor(public readonly id: string, public meta: EventMetaData) {}
}
