import {
  Controller,
  Logger,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
  Query,
  ValidationPipe,
} from '@nestjs/common';
import { TaskService } from '../services/task.service';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiOkResponse,
} from '@nestjs/swagger';
import { BreadController } from '../../shared/controllers/bread.controller';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { ParseIntOptionalPipe } from '../../shared/pipes/parse-int-optional.pipe';
import { ITask } from '@daypaio/domain-events/tasks';
import { TaskDTO } from '../dtos/task.dto';
import { Task } from '../models/task.model';

@ApiTags('tasks')
@ApiBearerAuth()
@Controller('tasks')
export class TaskController {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: TaskService) { }

  @ApiOperation({
    summary: 'Browse Tasks',
    description: 'Get all tasks',
  })
  @ApiOkResponse({
    description: 'The tasks have successfully been queried',
  })
  @Get()
  browse(
    @GetUser() user: LoggedInUser,
    @Query('limit', ParseIntOptionalPipe) limit: number = 0,
    @Query('offset', ParseIntOptionalPipe) offset: number = 0,
  ) {
    return this.service.browse(
      limit,
      offset,
      user.generateMeta(),
    );
  }

  @ApiOperation({
    summary: 'Browse Tasks by Person ID',
    description: 'Get all tasks by Person ID',
  })
  @ApiOkResponse({
    description: 'The tasks have successfully been queried',
  })
  @Get('persons/:personID')
  browseByPerson(
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
    @Query('limit', ParseIntOptionalPipe) limit: number = 0,
    @Query('offset', ParseIntOptionalPipe) offset: number = 0,
  ) {

    return this.service.browseByPerson(
      personID,
      limit,
      offset,
      user.generateMeta(),
    );
  }

  @ApiOperation({
    summary: 'Browse Tasks by Organization ID',
    description: 'Get all tasks by Organization ID',
  })
  @ApiOkResponse({
    description: 'The tasks have successfully been queried',
  })
  @Get('organizations/:organizationID')
  browseByOrganization(
    @Param('organizationID') organizationID: string,
    @GetUser() user: LoggedInUser,
    @Query('limit', ParseIntOptionalPipe) limit: number = 0,
    @Query('offset', ParseIntOptionalPipe) offset: number = 0,
  ) {

    return this.service.browseByOrganization(
      organizationID,
      limit,
      offset,
      user.generateMeta(),
    );
  }

  @ApiOperation({
    summary: 'Read Task',
    description: 'Get a single task',
  })
  @ApiOkResponse({
    description: 'The task has successfully been found',
    type: Task,
  })
  @Get(':id')
  read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.read(id, user.generateMeta());
  }

  @ApiOperation({
    summary: 'Edit Task',
    description: 'Edit single task',
  })
  @ApiOkResponse({
    description: 'The task editing event has successfully been triggered',
  })
  @Patch(':id')
  editTask(
    @Param('id') id: string,
    @Body('changes') task: Partial<TaskDTO>,
    @Body('oldTask') oldTask: TaskDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    return this.service.edit(id, task, oldTask, user.generateMeta());
  }

  @ApiOperation({
    summary: 'Add Task',
    description: 'Create a single task',
  })
  @ApiOkResponse({
    description: 'The task adding event has successfully been triggered',
  })
  @Post()
  add(
    @Body() task: TaskDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    return this.service.add(task, user.generateMeta());
  }

  @ApiOperation({
    summary: 'Delete Task',
    description: 'Delete a single task',
  })
  @ApiOkResponse({
    description: 'The task deleting event has successfully been triggered',
  })
  @Delete(':id')
  delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    return this.service.delete(id, user.generateMeta());
  }

}
