import { TenantDeletedEventHandler } from './tenant-deleted.event-handler';
import { TaskEditedEventHandler } from './task-edited.event-handler';
import { TaskDeletedEventHandler } from './task-deleted.event-handler';
import { TaskAddedEventHandler } from './task-added.event-handler';

export const EventHandlers = [
  TaskEditedEventHandler,
  TaskDeletedEventHandler,
  TaskAddedEventHandler,
  TenantDeletedEventHandler,
];
