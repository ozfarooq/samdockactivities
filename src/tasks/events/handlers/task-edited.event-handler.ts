import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { TaskRepository } from '../../repositories/task.repository';
import { TaskEditedEvent } from '@daypaio/domain-events/tasks';

@EventsHandler(TaskEditedEvent)
export class TaskEditedEventHandler implements IEventHandler<TaskEditedEvent> {

  constructor(
    private readonly repo: TaskRepository,
  ) {}

  async handle(event: TaskEditedEvent) {

    const { _id, task, meta } = event;

    this.repo.edit(_id, task, meta);

  }
}
