import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TenantDeletedEvent } from '@daypaio/domain-events/tenants';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task.model';

@EventsHandler(TenantDeletedEvent)
export class TenantDeletedEventHandler
  implements IEventHandler<TenantDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private TaskService: TaskService,
  ) { }
  async handle(event: TenantDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id } = event;

    const meta = new EventMetaData(_id, null);
    try {
      const activities = await this.TaskService.browse(0, 0, meta);

      await Promise.all(
        activities.map((task: Task) => this.TaskService.delete(task._id, meta)),
      );
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
