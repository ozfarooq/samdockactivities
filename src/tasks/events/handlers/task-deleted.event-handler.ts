import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { TaskRepository } from '../../repositories/task.repository';
import { TaskDeletedEvent } from '@daypaio/domain-events/tasks';

@EventsHandler(TaskDeletedEvent)
export class TaskDeletedEventHandler implements IEventHandler<TaskDeletedEvent> {

  constructor(
    private readonly repo: TaskRepository,
  ) {}

  async handle(event: TaskDeletedEvent) {

    const { _id, meta } = event;

    this.repo.delete(_id, meta);

  }
}
