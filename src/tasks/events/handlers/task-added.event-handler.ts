import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { TaskRepository } from '../../repositories/task.repository';
import { TaskAddedEvent } from '@daypaio/domain-events/tasks';

@EventsHandler(TaskAddedEvent)
export class TaskAddedEventHandler implements IEventHandler<TaskAddedEvent> {

  constructor(
    private readonly repo: TaskRepository,
  ) {}

  async handle(event: TaskAddedEvent) {

    const { task, meta } = event;

    this.repo.add(task, meta);

  }
}
