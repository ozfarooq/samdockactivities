import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { TaskController } from './controllers/task.controller';
import { TaskRepository } from './repositories/task.repository';
import { TaskService } from './services/task.service';
import { QueryHandlers } from './queries/handlers';
import { CommandHandlers } from './commands/handlers';
import { EventHandlers } from './events/handlers';
import { Task } from './models/task.model';

@Module({
  imports: [
    TypegooseModule.forFeature([Task]),
  ],
  controllers: [
    TaskController,
  ],
  providers: [
    TaskService,
    TaskRepository,
    ...QueryHandlers,
    ...CommandHandlers,
    ...EventHandlers,
  ],
})
export class TaskModule {}
