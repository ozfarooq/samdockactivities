import { InjectModel } from 'nestjs-typegoose';
import { Task } from '../models/task.model';
import { BreadRepository, PaginationResult } from '../../shared/bread.respository';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { Model } from 'mongoose';

export class TaskRepository extends BreadRepository<Task> {

  constructor(
    @InjectModel(Task) model: Model<any>,
  ) {
    super(model);
  }

  async browseByPersons(
    _personID: string,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Task> | Task[]> {
    this.logger.verbose('FIND BY PERSON');
    const { _tenantID } = meta;
    try {
      if (limit || offset) {
        const data = await this.model
          .find({ _tenantID, _personID })
          .sort({ timestamp: 1 })
          .skip(offset)
          .limit(limit)
          .lean();

        return {
          limit,
          offset,
          data,
        } as PaginationResult<Task>;
      }

      const result = this.model.find({ _tenantID, _personID }).sort({ timestamp: 1 }).lean();

      return result as unknown as Task[];
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browseByOrganizations(
    _organizationID: string,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Task> | Task[]> {
    this.logger.verbose('FIND BY ORGANIZATION');
    const { _tenantID } = meta;
    try {
      if (limit || offset) {
        const data = await this.model
          .find({ _tenantID, _organizationID })
          .sort({ timestamp: 1 })
          .skip(offset)
          .limit(limit)
          .lean();

        return {
          limit,
          offset,
          data,
        } as PaginationResult<Task>;
      }

      const result = this.model.find({ _tenantID, _organizationID }).sort({ timestamp: 1 }).lean();

      return result as unknown as Task[];
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

}
