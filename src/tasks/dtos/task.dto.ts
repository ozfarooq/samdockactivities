import { ITask, TaskPriority } from '@daypaio/domain-events/tasks';
import { plainToClassFromExist } from 'class-transformer';
import {
  IsString,
  IsOptional,
  IsNumber,
} from 'class-validator';

export class TaskDTO implements ITask {

  constructor(data?: any) {
    plainToClassFromExist(this, data);
  }

  @IsString()
  _id: string;

  @IsString()
  @IsOptional()
  _userID: string;

  @IsString()
  @IsOptional()
  _personID: string;

  @IsString()
  @IsOptional()
  _organizationID: string;

  @IsString()
  @IsOptional()
  _dealID: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  priority: TaskPriority;

  @IsNumber()
  @IsOptional()
  dueDate: number;

  @IsNumber()
  @IsOptional()
  startDate: number;

  @IsNumber()
  @IsOptional()
  completionDate: number;

  @IsNumber()
  @IsOptional()
  createdAt: number;

  @IsNumber()
  @IsOptional()
  updatedAt: number;

}
