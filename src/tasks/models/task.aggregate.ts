import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';
import { ITask, TaskPriority } from '@daypaio/domain-events/tasks';

export class TaskAggregate extends AggregateRoot implements ITask {
  _id: string;
  _userID: string;
  _personID: string;
  _organizationID: string;
  _dealID: string;
  name: string;
  description: string;
  priority: TaskPriority;
  dueDate: number;
  startDate: number;
  completionDate: number;
  createdAt: number;
  updatedAt: number;

  constructor(data?: Partial<ITask>) {
    super();

    plainToClassFromExist(this, data);
  }

}
