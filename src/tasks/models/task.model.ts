import { prop, index } from '@typegoose/typegoose';
import { plainToClassFromExist } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { ITask, TaskPriority } from '@daypaio/domain-events/tasks';

@index({ _tenantID: 1 })
@index({ _tenantID: 1, _id: 1 })
@index({ _tenantID: 1, _personID: 1 })
@index({ _tenantID: 1, _organizationID: 1 })
export class Task implements ITask {
  @ApiProperty({
    description: 'The related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The ID of the task',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiProperty({
    description: 'The related person',
    required: false,
  })
  @prop()
  _personID: string;

  @ApiProperty({
    description: 'The related organization',
    required: false,
  })
  @prop()
  _organizationID: string;

  @ApiProperty({
    description: 'The related deal',
    required: false,
  })
  @prop()
  _dealID: string;

  @ApiProperty({
    description: 'The related user',
    required: false,
  })
  @prop()
  _userID: string;

  @ApiProperty({
    description: 'Name of the task',
    required: false,
  })
  @prop()
  name: string;

  @ApiProperty({
    description: 'Description of the task',
    required: false,
  })
  @prop()
  description: string;

  @ApiProperty({
    description: 'Priority of the task',
    required: false,
  })
  @prop()
  priority: TaskPriority;

  @ApiProperty({
    description: 'Due date of the task',
    required: false,
  })
  @prop()
  dueDate: number;

  @ApiProperty({
    description: 'Start date of the task',
    required: false,
  })
  @prop()
  startDate: number;

  @ApiProperty({
    description: 'Completion date of the task',
    required: false,
  })
  @prop()
  completionDate: number;

  @prop()
  createdAt: number;

  @prop()
  updatedAt: number;

  constructor(data?: Partial<Task>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
