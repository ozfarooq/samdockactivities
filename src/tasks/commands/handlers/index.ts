import { AddTaskCommandHandler } from './add-task.command-handler';
import { EditTaskCommandHandler } from './edit-task.command-handler';
import { DeleteTaskCommandHandler } from './delete-task.command-handler';

export const CommandHandlers = [
  AddTaskCommandHandler,
  DeleteTaskCommandHandler,
  EditTaskCommandHandler,
];
