import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddTaskCommand } from '../impl/add-task.command';
import { TaskAggregate } from '../../models/task.aggregate';
import { TaskAddedEvent } from '@daypaio/domain-events/tasks';

@CommandHandler(AddTaskCommand)
export class AddTaskCommandHandler implements ICommandHandler<AddTaskCommand> {

  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AddTaskCommand) {

    const { task, meta } = command;

    const taskAggregate = this.publisher.mergeObjectContext(
      new TaskAggregate(task),
    );

    const event = new TaskAddedEvent(
      task,
      meta,
    );
    taskAggregate.apply(
      event,
    );

    taskAggregate.commit();
  }
}
