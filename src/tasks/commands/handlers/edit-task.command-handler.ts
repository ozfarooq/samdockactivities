import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { TaskAggregate } from '../../models/task.aggregate';
import { EditTaskCommand } from '../impl/edit-task.command';
import { TaskEditedEvent } from '@daypaio/domain-events/tasks';

@CommandHandler(EditTaskCommand)
export class EditTaskCommandHandler implements ICommandHandler<EditTaskCommand> {

  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: EditTaskCommand) {

    const { _id, task, oldTask, meta } = command;

    task.updatedAt = meta.timestamp;

    const taskAggregate = this.publisher.mergeObjectContext(
      new TaskAggregate(task),
    );

    const event = new TaskEditedEvent(
      _id,
      task,
      oldTask,
      meta,
    );
    taskAggregate.apply(
      event,
    );

    taskAggregate.commit();
  }
}
