import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { TaskAggregate } from '../../models/task.aggregate';
import { DeleteTaskCommand } from '../impl/delete-task.command';
import { TaskDeletedEvent } from '@daypaio/domain-events/tasks';

@CommandHandler(DeleteTaskCommand)
export class DeleteTaskCommandHandler implements ICommandHandler<DeleteTaskCommand> {

  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteTaskCommand) {

    const { _id, meta } = command;

    const taskAggregate = this.publisher.mergeObjectContext(
      new TaskAggregate({ _id }),
    );

    const event = new TaskDeletedEvent(
      _id,
      meta,
    );
    taskAggregate.apply(
      event,
    );

    taskAggregate.commit();
  }
}
