import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ITask } from '@daypaio/domain-events/tasks';

export class AddTaskCommand implements ICommand {
  constructor(public task: ITask, public meta: EventMetaData) {}
}
