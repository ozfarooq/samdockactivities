import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class DeleteTaskCommand implements ICommand {
  constructor(public _id: string, public meta: EventMetaData) {}
}
