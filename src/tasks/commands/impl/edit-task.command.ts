import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ITask } from '@daypaio/domain-events/tasks';

export class EditTaskCommand implements ICommand {
  constructor(
    public _id: string,
    public task: Partial<ITask>,
    public oldTask: ITask,
    public meta: EventMetaData,
  ) {}
}
