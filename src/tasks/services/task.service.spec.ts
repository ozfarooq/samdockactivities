import { Test, TestingModule } from '@nestjs/testing';
import { TaskService } from './task.service';
import { getModelToken } from 'nestjs-typegoose';
import { CqrsModule } from '@nestjs/cqrs';

const mockPersonModel = () => ({});

describe('TaskService', () => {
  let service: TaskService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        TaskService,
        {
          provide: getModelToken('Person'),
          useFactory: mockPersonModel,
        },
      ],
    }).compile();

    service = module.get<TaskService>(TaskService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
