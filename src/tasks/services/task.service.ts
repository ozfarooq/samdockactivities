import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTaskCommand } from '../commands/impl/add-task.command';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { BreadService } from '../../shared/services/bread.service';
import { BrowseTasksQuery } from '../queries/impl/browse-tasks.query';
import { ReadTaskQuery } from '../queries/impl/read-task.query';
import { EditTaskCommand } from '../commands/impl/edit-task.command';
import { DeleteTaskCommand } from '../commands/impl/delete-task.command';
import { ITask } from '@daypaio/domain-events/tasks';

@Injectable()
export class TaskService extends BreadService<ITask> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  browse(limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(new BrowseTasksQuery(null, limit, offset, meta));
  }

  browseByPerson(personID: string, limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(
      new BrowseTasksQuery({ _personID: personID }, limit, offset, meta),
    );
  }

  browseByOrganization(organizationID: string, limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(
      new BrowseTasksQuery({ _organizationID: organizationID }, limit, offset, meta),
    );
  }

  read(id: string, meta: EventMetaData) {
    return this.executeQuery(new ReadTaskQuery(id, meta));
  }

  edit(id: string, task: Partial<ITask>, oldTask: ITask, meta: EventMetaData) {
    return this.executeCommand(new EditTaskCommand(id, task, oldTask, meta));
  }

  add(task: ITask, meta: EventMetaData) {
    return this.executeCommand(new AddTaskCommand(task, meta));
  }

  delete(id: string, meta: EventMetaData) {
    return this.executeCommand(new DeleteTaskCommand(id, meta));
  }

}
