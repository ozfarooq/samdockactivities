import { LoggedInUser } from '../decorators/get-user.decorator';

export interface BreadController<T> {
  browse(...args: any[]): Promise<T[]>;

  read(id: string, user: LoggedInUser): Promise<T>;

  edit(id: string, object: any, user: LoggedInUser): Promise<void>;

  add(object: T, user: LoggedInUser): Promise<void>;

  delete(id: string, user: LoggedInUser): Promise<void>;
}
