import {
  createParamDecorator,
  UnauthorizedException,
  Logger,
} from '@nestjs/common';
import * as jwt_decode from 'jwt-decode';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class LoggedInUser {
  constructor(public _tenantID: string, public _id: string, public email: string) {}

  generateMeta() {
    return new EventMetaData(this._tenantID, this._id);
  }
}

export const GetUser = createParamDecorator(
  (_data, ctx): LoggedInUser => {
    const req = ctx.switchToHttp().getRequest();
    return _getUser(req);
  },
);

export const GetWsUser = createParamDecorator(
  (_data, ctx): LoggedInUser => {
    const req = ctx.switchToWs().getData();
    return _getUser(req);
  },
);

const _getUser = (req) => {
  const logger = new Logger('GetUserDecorator');
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer ')
  ) {
    const token = req.headers.authorization.substring(
      7,
      req.headers.authorization.length,
    );
    try {
      const parsedUser: any = jwt_decode(token);
      const { _tenantID, _id, email } = parsedUser;
      return new LoggedInUser(_tenantID, _id, email);
    } catch (error) {
      logger.error('Could not get logged in user from token', error.stack);
      throw new UnauthorizedException();
    }
  } else {
    throw new UnauthorizedException();
  }
};
