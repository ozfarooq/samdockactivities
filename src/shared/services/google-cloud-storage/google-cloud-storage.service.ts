import { Injectable, NotFoundException } from '@nestjs/common';
import { Storage } from '@google-cloud/storage';

@Injectable()
export class GoogleCloudStorageService {
  keyFile: string = './service-accounts/daypaio-57502c37f38f.json';
  storage: Storage = new Storage({ keyFilename: this.keyFile, projectId: 'daypaio' });

  async createBucket(bucketName: string) {
    await this.storage.createBucket(bucketName, {
      location: 'EUROPE-WEST3',
      cors: [
        {
          origin: ['*'],
          responseHeader: ['Authorization', 'Content-Type'],
          method: [
            'GET',
            'OPTIONS',
          ],
          maxAgeSeconds: 10,
        },
      ],
    });
  }

  async uploadFile(
    file: any,
    fileName: string,
    bucketName: string,
    makePublic = false,
  ): Promise<void> {
    const isBucketExisting = (await this.storage.bucket(bucketName).exists())[0];

    if (!isBucketExisting) {
      await this.createBucket(bucketName);
    }

    const uploadedFile = this.storage.bucket(bucketName).file(fileName);

    const stream = uploadedFile.createWriteStream({ contentType: file.mimetype });

    await new Promise((resolve, reject) => {
      stream.on('error', (err: Error) => {
        reject(err);
      });
      stream.on('finish', (res) => {
        resolve(res);
      });
      stream.end(file.buffer);
    });

    if (makePublic) {
      await uploadedFile.makePublic();
    }

  }

  async deleteFile(fileName: string, bucketName: string) {
    try {
      await this.storage.bucket(bucketName).file(fileName).delete();
    } catch (e) {
      throw new NotFoundException();
    }
  }

}
