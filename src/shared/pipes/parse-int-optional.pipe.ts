import { Injectable, PipeTransform, ParseIntPipe, ArgumentMetadata } from '@nestjs/common';

@Injectable()
export class ParseIntOptionalPipe extends ParseIntPipe implements PipeTransform<string> {

  async transform(value: string, metadata: ArgumentMetadata): Promise<number> {
    const isNumeric =
      ['string', 'number'].includes(typeof value) &&
      !isNaN(parseFloat(value)) &&
      isFinite(value as any);
    if (!isNumeric) {
      return 0;
    }
    return parseInt(value, 10);
  }

}
