import {
  Logger,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { Model } from 'mongoose';

export class BreadRepository<T> {
  protected logger: Logger;

  constructor(protected readonly model: Model<any>) {
    this.logger = new Logger(`${this.model.modelName}Repository`);
  }

  protected generateErrorMessage(
    error: any,
    operation: string,
    id?: string,
    data?: any,
  ) {
    const errorMessage = error.message;
    const operationMessage = `${
      this.model.modelName
    } could not be ${operation.toLowerCase()}ed}`;
    const idMessage = id ? `ID: ${id}` : '';
    const dataMessage = data ? JSON.stringify(data) : '';
    return {
      error: operationMessage + errorMessage,
      data: idMessage + dataMessage,
      verbose: `${error.constructor.name} \n
        ${operationMessage} \n
        ${errorMessage} \n
        ${idMessage} \n
        ${dataMessage}`,
    };
  }

  async add(data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('CREATE');
    console.table(data);

    try {
      data._tenantID = meta._tenantID;

      await this.model.create(data);
    } catch (error) {
      const message = this.generateErrorMessage(
        error,
        'create',
        data._id,
        data,
      );
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async edit(id: string, data: any, meta: EventMetaData): Promise<void> {
    this.logger.verbose('EDIT');
    console.table({ data, _id: id });
    try {
      const result = await this.model.updateOne(
        { _tenantID: meta._tenantID, _id: id },
        { $set: data },
      );
      const { n, nModified } = result;
      if (nModified > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (nModified < 1) {
        this.logger.verbose(
          `Document for ${this.model.modelName} matched but information was the same`,
        );
        return;
      }
      throw new InternalServerErrorException(
        `Failed editing model for ${this.model.modelName}: result: ${result}`,
      );
    } catch (error) {
      const message = this.generateErrorMessage(error, 'updated', id, data);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async delete(id: string, meta: EventMetaData): Promise<void> {
    this.logger.verbose('DELETE');

    try {
      const result = await this.model.deleteOne({
        _tenantID: meta._tenantID,
        _id: id,
      });
      const { n, deletedCount } = result;
      if (deletedCount > 0) {
        return;
      }
      if (n < 1) {
        throw new NotFoundException();
      }
      if (deletedCount < 1) {
        this.logger.verbose(
          `${this.model.modelName} was found with id: ${id} but could not be deleted, with result: ${result} `,
        );
      }
      throw new InternalServerErrorException();
    } catch (error) {
      const message = this.generateErrorMessage(error, 'delete', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browse(
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<T> | T[]> {
    this.logger.verbose('FIND');
    try {
      if (limit || offset) {
        const data = await this.model
          .find({ _tenantID: meta._tenantID })
          .sort({ timestamp: 1 })
          .skip(offset)
          .limit(limit)
          .lean();

        return {
          limit,
          offset,
          data,
        };
      }

      const result = this.model.find({ _tenantID: meta._tenantID }).sort({ timestamp: 1 }).lean();

      return result;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async read(id: string, meta: EventMetaData): Promise<T> {
    this.logger.verbose('FIND BY ID');
    console.table({ id, _tenantID: meta._tenantID });
    try {
      const result = await this.model.findOne(
        { _tenantID: meta._tenantID, _id: id },
      ).lean();
      if (result == null) {
        throw new NotFoundException();
      }
      return result as T;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }
}

export interface PaginationResult<T> {
  data: T[];
  offset: number;
  limit: number;
}
