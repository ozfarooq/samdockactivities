import * as path from 'path';
import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { HealthcheckModule } from './shared/healthcheck/healthcheck.module';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { EventStoreCqrsModule } from 'nestjs-eventstore';
import { eventStoreBusConfig } from './event-bus.provider';
import { ActivityModule } from './activities/activity.module';
import { TaskModule } from './tasks/task.module';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    TypegooseModule.forRootAsync({
      useFactory: async (config: ConfigService) => ({ uri: config.get('database.mongoConnectionString') }),
      inject: [ConfigService],
    }),
    EventStoreCqrsModule.forRootAsync(
      {
        useFactory: async (config: ConfigService) => {
          return {
            connectionSettings: config.get('eventstore.connectionSettings'),
            endpoint: config.get('eventstore.tcpEndpoint'),
          };
        },
        inject: [ConfigService],
      },
      eventStoreBusConfig,
    ),
    HealthcheckModule,
    ActivityModule,
    TaskModule,
  ],
})
export class AppModule {}
