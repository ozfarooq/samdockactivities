import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupDotenv } from './dotenv.setup';
import { Logger, ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';
import { json } from 'express';

async function bootstrap() {
  const logger = new Logger('bootstrap');

  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const domainName = process.env.DOMAIN.toLowerCase() || 'activities';
  app.setGlobalPrefix(`api/${domainName}`);

  app.use(json({ limit: '50mb' }));

  const options = new DocumentBuilder()
    .setTitle(domainName)
    .setDescription(`${domainName} Microservice`)
    .setVersion('0.1.0')
    .setBasePath(`/api/${domainName}/`)
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(`/api/${domainName}/docs`, app, document);
  app.enableCors();

  const port = process.env.PORT || 3000;
  await app.listen(port);

  logger.log(`Application listening on port ${port}`);
}

setupDotenv();
bootstrap();
