import { InjectModel } from 'nestjs-typegoose';
import { Activity } from '../models/activity.model';
import { BreadRepository, PaginationResult } from '../../shared/bread.respository';
import { ReturnModelType } from '@typegoose/typegoose';
import { EventMetaData } from '@daypaio/domain-events/shared';

export class ActivityRepository extends BreadRepository<Activity> {

  constructor(
    @InjectModel(Activity) model: ReturnModelType<any>,
  ) {
    super(model);
  }

  async browseBy(
    filter: object,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Activity> | Activity[]> {
    const { _tenantID } = meta;
    try {
      if (limit || offset) {
        const data = await this.model
          .find({ _tenantID, ...filter })
          .sort({ timestamp: 1 })
          .skip(offset)
          .limit(limit)
          .lean();

        return {
          limit,
          offset,
          data,
        } as PaginationResult<Activity>;
      }

      const result = await this.model.find({ _tenantID, ...filter })
        .sort({ timestamp: 1 })
        .lean();

      return result as Activity[];
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find');
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async browseByPersons(
    _personID: string,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Activity> | Activity[]> {
    this.logger.verbose('FIND BY PERSON');
    return this.browseBy({ _personIDs: _personID }, limit, offset, meta);
  }

  async browseByOrganizations(
    _organizationID: string,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Activity> | Activity[]> {
    this.logger.verbose('FIND BY ORGANIZATION');
    return this.browseBy({ _organizationIDs: _organizationID }, limit, offset, meta);
  }

  async browseByTask(
    _taskID: string,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Activity> | Activity[]> {
    this.logger.verbose('FIND BY TASK');
    return this.browseBy({ _taskID }, limit, offset, meta);
  }

  async browseByDeal(
    _dealID: string,
    limit: number,
    offset: number,
    meta: EventMetaData,
  ): Promise<PaginationResult<Activity> | Activity[]> {
    this.logger.verbose('FIND BY DEAL');
    return this.browseBy({ _dealID }, limit, offset, meta);
  }

}
