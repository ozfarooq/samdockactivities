import { InjectModel } from 'nestjs-typegoose';
import { BreadRepository } from '../../shared/bread.respository';
import { ReturnModelType } from '@typegoose/typegoose';
import { MailActivitySettings } from '../models/mail-activity-settings.model';
import { NotFoundException } from '@nestjs/common';

export class MailActivitySettingsRepository extends BreadRepository<MailActivitySettings> {

  constructor(
    @InjectModel(MailActivitySettings) model: ReturnModelType<typeof MailActivitySettings>,
  ) {
    super(model);
  }

  async read(id: string) {
    try {
      const result = await this.model.findOne(
        { _id: id },
      ).lean();
      if (result === null) {
        throw new NotFoundException();
      }
      return result as MailActivitySettings;
    } catch (error) {
      const message = this.generateErrorMessage(error, 'find', id);
      this.logger.verbose(message.verbose);
      throw error;
    }
  }

  async delete(id: string): Promise<void> {
    await this.model.deleteOne({
      _id: id,
    });
  }

  async addWhitelistEntry(tenantID: string, email: string, userID?: string) {
    await this.model.updateOne(
      { _id: tenantID, 'whitelist.email': { $nin: [email] } },
      { $push: { whitelist: { email, userID } as never } },
      { upsert: true },
    );
  }

  async removeWhitelistEntryByMail(tenantID: string, email: string) {
    await this.model.updateOne(
      { _id: tenantID },
      { $pull: { whitelist: { email } as never } },
      { multi: true },
    );
  }

  async removeWhitelistEntryByUserID(tenantID: string, userID: string) {
    await this.model.updateOne(
      { _id: tenantID },
      { $pull: { whitelist: { userID } as never } },
      { multi: true },
    );
  }

}
