import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ActivityRepository } from '../../repositories/activity.repository';
import { ActivityEditedEvent } from '@daypaio/domain-events/activities';

@EventsHandler(ActivityEditedEvent)
export class ActivityEditedEventHandler implements IEventHandler<ActivityEditedEvent> {

  constructor(
    private readonly repo: ActivityRepository,
  ) {}

  async handle(event: ActivityEditedEvent) {

    const { _id, activity, meta } = event;

    this.repo.edit(_id, activity, meta);

  }
}
