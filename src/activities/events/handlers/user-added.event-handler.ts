import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { MailActivitySettingsRepository } from '../../repositories/mail-activity-settings.repository';
import { UserAddedEvent } from '@daypaio/domain-events/users';

@EventsHandler(UserAddedEvent)
export class UserAddedEventHandler implements IEventHandler<UserAddedEvent> {

  constructor(
    private readonly repo: MailActivitySettingsRepository,
  ) {}

  async handle(event: UserAddedEvent) {

    const { user, meta } = event;

    await this.repo.addWhitelistEntry(meta._tenantID, user.email, user._id);
  }
}
