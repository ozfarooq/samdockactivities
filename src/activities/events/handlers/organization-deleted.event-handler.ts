import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { OrganizationDeletedEvent } from '@daypaio/domain-events/organizations';
import { Activity } from '../../models/activity.model';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';

@EventsHandler(OrganizationDeletedEvent)
export class OrganizationDeletedEventHandler
  implements IEventHandler<OrganizationDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    @InjectModel(Activity)
    private model: ReturnModelType<any>,
  ) { }
  async handle(event: OrganizationDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;

    try {
      return await this.model.updateMany(
        { 'data._organizationID': _id, _tenantID: meta._tenantID },
        { isLinkedEntityDeleted: true  },
      );
    } catch (error) {
      this.logger.error(`Failed to delete handle deleting organization activities of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
