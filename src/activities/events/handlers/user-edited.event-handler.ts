import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { MailActivitySettingsRepository } from '../../repositories/mail-activity-settings.repository';
import { UserEditedEvent } from '@daypaio/domain-events/users';

@EventsHandler(UserEditedEvent)
export class UserEditedEventHandler implements IEventHandler<UserEditedEvent> {

  constructor(
    private readonly repo: MailActivitySettingsRepository,
  ) {}

  async handle(event: UserEditedEvent) {

    const { user, meta } = event;

    if (!user.email) {
      return;
    }

    await this.repo.removeWhitelistEntryByUserID(meta._tenantID, user._id);
    await this.repo.addWhitelistEntry(meta._tenantID, user.email, user._id);
  }
}
