import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ActivityRepository } from '../../repositories/activity.repository';
import { ActivityDeletedEvent } from '@daypaio/domain-events/activities';

@EventsHandler(ActivityDeletedEvent)
export class ActivityDeletedEventHandler implements IEventHandler<ActivityDeletedEvent> {

  constructor(
    private readonly repo: ActivityRepository,
  ) {}

  async handle(event: ActivityDeletedEvent) {

    const { _id, meta } = event;

    this.repo.delete(_id, meta);

  }
}
