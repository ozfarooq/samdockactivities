import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TenantDeletedEvent } from '@daypaio/domain-events/tenants';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { ActivityService } from '../../services/activity.service';
import { Activity } from '../../models/activity.model';
import { MailActivitySettingsRepository } from '../../repositories/mail-activity-settings.repository';

@EventsHandler(TenantDeletedEvent)
export class TenantDeletedEventHandler
  implements IEventHandler<TenantDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    private activityService: ActivityService,
    private mailActivtiySettingsRepo: MailActivitySettingsRepository,
  ) { }
  async handle(event: TenantDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id } = event;

    const meta = new EventMetaData(_id, null);
    try {
      await this.mailActivtiySettingsRepo.delete(_id);
      const activities = await this.activityService.browse(0, 0, meta);

      await Promise.all(
        activities.map((activity: Activity) => this.activityService.delete(activity._id, meta)),
      );
    } catch (error) {
      this.logger.error(`Failed to delete deal of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
