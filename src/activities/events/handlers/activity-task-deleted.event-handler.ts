import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TaskDeletedEvent } from '@daypaio/domain-events/tasks';
import { Activity } from '../../models/activity.model';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';

@EventsHandler(TaskDeletedEvent)
export class ActivityTaskDeletedEventHandler
  implements IEventHandler<TaskDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    @InjectModel(Activity)
    private model: ReturnModelType<any>,
  ) { }
  async handle(event: TaskDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;

    try {
      return await this.model.updateMany(
        { 'data._taskID': _id, _tenantID: meta._tenantID },
        { isLinkedEntityDeleted: true },
      );
    } catch (error) {
      this.logger.error(`Failed to handle deleting task activities of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
