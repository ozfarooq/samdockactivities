import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { MailActivitySettingsRepository } from '../../repositories/mail-activity-settings.repository';
import { UserDeletedEvent } from '@daypaio/domain-events/users';

@EventsHandler(UserDeletedEvent)
export class UserDeletedEventHandler implements IEventHandler<UserDeletedEvent> {

  constructor(
    private readonly repo: MailActivitySettingsRepository,
  ) {}

  async handle(event: UserDeletedEvent) {

    const { _id, meta } = event;

    await this.repo.removeWhitelistEntryByUserID(meta._tenantID, _id);
  }
}
