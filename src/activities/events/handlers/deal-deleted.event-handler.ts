import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { DealDeletedEvent, LeadDeletedEvent } from '@daypaio/domain-events/deals';
import { Activity } from '../../models/activity.model';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';

@EventsHandler(DealDeletedEvent, LeadDeletedEvent)
export class DealDeletedEventHandler
  implements IEventHandler<DealDeletedEvent | LeadDeletedEvent> {
  private logger = new Logger(this.constructor.name);
  constructor(
    @InjectModel(Activity)
    private model: ReturnModelType<any>,
  ) { }
  async handle(event: DealDeletedEvent | LeadDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}}`);
    const { _id, meta } = event;

    try {
      return await this.model.updateMany(
        { _tenantID: meta._tenantID, $or: [{ 'data._dealID': _id }, { 'data._leadID': _id }] },
        { isLinkedEntityDeleted: true },
      );
    } catch (error) {
      this.logger.error(`Failed to handle deleting deal activities of id ${_id}`);
      this.logger.log(error.message);
      this.logger.debug(error.stack);
      // TODO retry event
    }
  }
}
