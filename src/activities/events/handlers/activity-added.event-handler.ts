import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { ActivityRepository } from '../../repositories/activity.repository';
import { ActivityAddedEvent } from '@daypaio/domain-events/activities';

@EventsHandler(ActivityAddedEvent)
export class ActivityAddedEventHandler implements IEventHandler<ActivityAddedEvent> {

  constructor(
    private readonly repo: ActivityRepository,
  ) {}

  async handle(event: ActivityAddedEvent) {

    const { activity, meta } = event;

    this.repo.add(activity, meta);

  }
}
