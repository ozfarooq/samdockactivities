import { TenantDeletedEventHandler } from './tenant-deleted.event-handler';
import { ActivityEditedEventHandler } from './activity-edited.event-handler';
import { ActivityDeletedEventHandler } from './activity-deleted.event-handler';
import { ActivityAddedEventHandler } from './activity-added.event-handler';
import { DealDeletedEventHandler } from './deal-deleted.event-handler';
import { PersonDeletedEventHandler } from './person-deleted.event-handler';
import { OrganizationDeletedEventHandler } from './organization-deleted.event-handler';
import { ActivityTaskDeletedEventHandler } from './activity-task-deleted.event-handler';
import { UserAddedEventHandler } from './user-added.event-handler';
import { UserDeletedEventHandler } from './user-deleted.event-handler';
import { UserEditedEventHandler } from './user-edited.event-handler';

export const EventHandlers = [
  ActivityEditedEventHandler,
  ActivityDeletedEventHandler,
  ActivityAddedEventHandler,
  TenantDeletedEventHandler,
  DealDeletedEventHandler,
  PersonDeletedEventHandler,
  OrganizationDeletedEventHandler,
  ActivityTaskDeletedEventHandler,
  UserAddedEventHandler,
  UserDeletedEventHandler,
  UserEditedEventHandler,
];
