import {
  Controller,
  Logger,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
  Query,
} from '@nestjs/common';
import { ActivityService } from '../services/activity.service';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { BreadController } from '../../shared/controllers/bread.controller';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { ParseIntOptionalPipe } from '../../shared/pipes/parse-int-optional.pipe';
import { IActivity } from '@daypaio/domain-events/activities';
import { ActivityDTO } from '../dtos/activity.dto';
import { Activity } from '../models/activity.model';

@ApiTags('activities')
@ApiBearerAuth()
@Controller('activities')
export class ActivityController implements BreadController<IActivity> {
  protected logger = new Logger(this.constructor.name);
  constructor(private service: ActivityService) { }

  @Get()
  @ApiOperation({
    summary: 'Browse Activities',
    description: 'Get all activities',
  })
  @ApiOkResponse({
    description: 'The activities have successfully been queried',
  })
  browse(
    @GetUser() user: LoggedInUser,
    @Query('limit', ParseIntOptionalPipe) limit: number = 0,
    @Query('offset', ParseIntOptionalPipe) offset: number = 0,
  ) {
    return this.service.browse(
      limit,
      offset,
      user.generateMeta(),
    );
  }

  @ApiOperation({
    summary: 'Browse Activities by Person ID',
    description: 'Get all activities by Person ID',
  })
  @ApiOkResponse({
    description: 'The activities have successfully been queried',
  })
  @Get('persons/:personID')
  browseByPerson(
    @Param('personID') personID: string,
    @GetUser() user: LoggedInUser,
    @Query('limit', ParseIntOptionalPipe) limit: number = 0,
    @Query('offset', ParseIntOptionalPipe) offset: number = 0,
  ) {

    return this.service.browseByPerson(
      personID,
      limit,
      offset,
      user.generateMeta(),
    );
  }

  @ApiOperation({
    summary: 'Browse Activities by Organization ID',
    description: 'Get all activities by Organization ID',
  })
  @ApiOkResponse({
    description: 'The activities have successfully been queried',
  })
  @Get('organizations/:organizationID')
  browseByOrganization(
    @Param('organizationID') organizationID: string,
    @GetUser() user: LoggedInUser,
    @Query('limit', ParseIntOptionalPipe) limit: number = 0,
    @Query('offset', ParseIntOptionalPipe) offset: number = 0,
  ) {

    return this.service.browseByOrganization(
      organizationID,
      limit,
      offset,
      user.generateMeta(),
    );
  }

  @ApiOperation({
    summary: 'Read Activity',
    description: 'Get a single activity',
  })
  @ApiOkResponse({
    description: 'The activity has successfully been found',
    type: Activity,
  })
  @Get(':id')
  read(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ) {
    return this.service.read(id, user.generateMeta());
  }

  @ApiOperation({
    summary: 'Edit Activity',
    description: 'Edit single activity',
  })
  @ApiOkResponse({
    description: 'The activity editing event has successfully been triggered',
  })
  @Patch(':id')
  edit(
    @Param('id') id: string,
    @Body() activity: Partial<ActivityDTO>,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    return this.service.edit(id, activity, user.generateMeta());
  }

  @Post()
  @ApiOperation({
    summary: 'Add Activity',
    description: 'Create a single activity',
  })
  @ApiOkResponse({
    description: 'The activity adding event has successfully been triggered',
  })
  @Post()
  add(
    @Body() activity: ActivityDTO,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    this.logger.log('ADD ACTIITY');
    return this.service.add(activity, user.generateMeta());
  }

  @ApiOperation({
    summary: 'Delete Activity',
    description: 'Delete a single activity',
  })
  @ApiOkResponse({
    description: 'The activity deleting event has successfully been triggered',
  })
  @Delete(':id')
  delete(
    @Param('id') id: string,
    @GetUser() user: LoggedInUser,
  ): Promise<void> {
    return this.service.delete(id, user.generateMeta());
  }

}
