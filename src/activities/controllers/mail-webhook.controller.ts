import { EventMetaData } from '@daypaio/domain-events/shared';
import {
  Controller,
  Logger,
  Post,
  Body,
  UnauthorizedException,
  HttpService,
} from '@nestjs/common';
import { MailActivitySettingsRepository } from '../repositories/mail-activity-settings.repository';
import * as moment from 'moment';
import { sign } from 'jsonwebtoken';
import { ActivityService } from '../services/activity.service';
import { ActivityDTO } from '../dtos/activity.dto';
import { nanoid } from 'nanoid';
import { IPerson } from '@daypaio/domain-events/persons';
import { IOrganization } from '@daypaio/domain-events/organizations';
import { simpleParser, ParsedMail, AddressObject } from 'mailparser';
import * as FormData from 'form-data';
import * as fs from 'fs';
import { decodeHeader } from 'libmime';
import * as addressparser from 'nodemailer/lib/addressparser';

@Controller('mail-webhook')
export class MailWebhookController {
  protected logger = new Logger(this.constructor.name);
  constructor(
    private mailActivitiySettingsRepo: MailActivitySettingsRepository,
    private httpService: HttpService,
    private activityService: ActivityService,
  ) { }

  @Post('bcc')
  async bccMail(@Body() body: any) {
    try {
      const raw = body[0].msys.relay_message;
      const { content } = raw;

      const _tenantID = raw.rcpt_to.split('@')[0];

      const jwt = this.createFakeJWT(_tenantID);

      const mail = await simpleParser(content.email_rfc822);

      const attachments = await this.uploadAttachments(mail, jwt);

      const parsed = {
        attachments,
        subject: mail.subject,
        from: mail.from?.value,
        to: (mail.to as AddressObject)?.value,
        cc: (mail.cc as AddressObject)?.value,
        text: mail.text,
        html: mail.html,
        type: 'outgoing',
        date: moment(mail.date).unix() * 1000,
        reporter: raw.friendly_from,
      };

      const meta = new EventMetaData(_tenantID, null);

      const { whitelist } = await this.mailActivitiySettingsRepo.read(_tenantID);

      const matchingWhitelistEntry = whitelist.find(entry => entry.email === parsed.reporter);
      if (!matchingWhitelistEntry) {
        throw new UnauthorizedException();
      }

      const persons = await this.httpService.get(
        `${process.env.CONTACTS_URL}/api/contacts/persons`,
        { headers: { Authorization: `Bearer ${jwt}` } },
      ).toPromise();

      const organizations = await this.httpService.get(
        `${process.env.CONTACTS_URL}/api/contacts/organizations`,
        { headers: { Authorization: `Bearer ${jwt}` } },
      ).toPromise();

      const addedOrganizationIDs: string[] = [];

      for (const person of persons.data as IPerson[]) {
        const lowerCase = person?.email?.toLowerCase();

        if (!lowerCase) {
          continue;
        }

        if (!parsed.to?.some(to => to.address?.toLowerCase() === lowerCase)) {
          continue;
        }
        const _organizationID = person.employers?.length === 1 &&
          person.employers[0]._organizationID;

        if (_organizationID) {
          addedOrganizationIDs.push(_organizationID);
        }

        const activity = new ActivityDTO({
          _organizationIDs: [_organizationID],
          _id: nanoid(),
          _userID: matchingWhitelistEntry.userID,
          _personIDs: [person._id],
          data: parsed,
          type: 'mail',
          timestamp: parsed.date,
          createdAt: Date.now(),
        });
        await this.activityService.add(activity, meta);
      }

      for (const organization of organizations.data as IOrganization[]) {
        const lowerCase = organization?.email?.toLowerCase();

        if (!lowerCase) {
          continue;
        }

        if (
          !parsed.to?.some(to => to.address?.toLowerCase() === lowerCase) ||
          addedOrganizationIDs.includes(organization._id)
        ) {
          continue;
        }
        const activity = new ActivityDTO({
          _id: nanoid(),
          _userID: matchingWhitelistEntry.userID,
          _organizationIDs: [organization._id],
          data: parsed,
          type: 'mail',
          timestamp: parsed.date,
          createdAt: Date.now(),
        });
        await this.activityService.add(activity, meta);
      }

      return parsed;
    } catch (error) {
      this.logger.error(error);
      return null;
    }

  }

  @Post('fwd')
  async fwdMail(@Body() body: any) {
    try {
      const raw = body[0].msys.relay_message;
      const { content } = raw;

      const _tenantID = raw.rcpt_to.split('@')[0];

      const jwt = this.createFakeJWT(_tenantID);

      const mail = await simpleParser(content.email_rfc822);

      const attachments = await this.uploadAttachments(mail, jwt);

      const lines = mail.text.split('\n').reverse();

      const headers = new Map();

      for (const line of lines) {
        let { key, value } = decodeHeader(line);
        if (!key || !value) {
          continue;
        }
        switch (key) {
          case 'to':
          case 'an':
            key = 'to';
            value = addressparser(value);
            break;
          case 'from':
          case 'von':
            key = 'from';
            value = addressparser(value);
            break;
          case 'cc':
            key = 'cc';
            value = addressparser(value);
            break;
          case 'sent':
          case 'sent on':
          case 'gesendet':
            key = 'date';
            break;
          default:
            break;
        }
        headers.set(key, value);
      }

      const parsed = {
        attachments,
        subject: headers.get('subject'),
        from: headers.get('from'),
        to: headers.get('to'),
        cc: headers.get('cc'),
        text: mail.text,
        html: mail.html,
        type: 'incoming',
        date: moment(mail.date).unix() * 1000,
        reporter: raw.friendly_from,
      };

      this.logger.log(JSON.stringify(parsed));

      const meta = new EventMetaData(_tenantID, null);

      const { whitelist } = await this.mailActivitiySettingsRepo.read(_tenantID);

      const matchingWhitelistEntry = whitelist.find(entry => entry.email === parsed.reporter);
      if (!matchingWhitelistEntry) {
        throw new UnauthorizedException();
      }

      const persons = await this.httpService.get(
        `${process.env.CONTACTS_URL}/api/contacts/persons`,
        { headers: { Authorization: `Bearer ${jwt}` } },
      ).toPromise();

      const organizations = await this.httpService.get(
        `${process.env.CONTACTS_URL}/api/contacts/organizations`,
        { headers: { Authorization: `Bearer ${jwt}` } },
      ).toPromise();

      const addedOrganizationIDs: string[] = [];

      for (const person of persons.data as IPerson[]) {
        const lowerCase = person?.email?.toLowerCase();

        if (!lowerCase) {
          continue;
        }

        if (!parsed.from?.some(from => from?.address?.toLowerCase() === lowerCase)) {
          continue;
        }
        const _organizationID = person.employers?.length === 1 &&
          person.employers[0]._organizationID;

        if (_organizationID) {
          addedOrganizationIDs.push(_organizationID);
        }

        const activity = new ActivityDTO({
          _organizationIDs: [_organizationID],
          _id: nanoid(),
          _userID: matchingWhitelistEntry.userID,
          _personIDs: [person._id],
          data: parsed,
          type: 'mail',
          timestamp: parsed.date,
          createdAt: Date.now(),
        });
        await this.activityService.add(activity, meta);
      }

      for (const organization of organizations.data as IOrganization[]) {
        const lowerCase = organization?.email?.toLowerCase();

        if (!lowerCase) {
          continue;
        }

        if (
          !parsed.from?.some(from => from?.address?.toLowerCase() === lowerCase) ||
          addedOrganizationIDs.includes(organization._id)
        ) {
          continue;
        }
        const activity = new ActivityDTO({
          _id: nanoid(),
          _userID: matchingWhitelistEntry.userID,
          _organizationIDs: [organization._id],
          data: parsed,
          type: 'mail',
          timestamp: parsed.date,
          createdAt: Date.now(),
        });
        await this.activityService.add(activity, meta);
      }

      return parsed;
    } catch (error) {
      this.logger.error(error);
      return null;
    }
  }

  createFakeJWT(_tenantID: string) {
    return sign({ _tenantID }, 'fake');
  }

  async uploadAttachments(mail: ParsedMail, jwt: string) {
    const result = [];
    const folder = nanoid();

    fs.mkdirSync(folder);

    for (const attachment of mail.attachments) {
      if (attachment.related) {
        continue;
      }

      fs.writeFileSync(`${folder}/${attachment.filename}`, attachment.content);

      const form = new FormData();

      form.append('attachment', fs.createReadStream(`${folder}/${attachment.filename}`));

      try {
        const res = await this.httpService.post(
          `${process.env.ATTACHMENTS_URL}/api/attachments`,
          form,
          {
            headers: {
              ...form.getHeaders(),
              Authorization: `Bearer ${jwt}`,
            },
          },
        ).toPromise();

        result.push(res.data);
      } catch (error) {
        this.logger.error(error);
      }
    }

    fs.rmdirSync(folder, { recursive: true });

    return result;
  }

}
