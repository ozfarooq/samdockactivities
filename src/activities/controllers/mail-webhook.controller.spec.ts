import { Test, TestingModule } from '@nestjs/testing';
import { MailWebhookController } from './mail-webhook.controller';
import * as fwdMail from './fwd-mail.json';
import * as bccMail from './bcc-mail.json';
import { MailActivitySettingsRepository } from '../repositories/mail-activity-settings.repository';
import { ActivityService } from '../services/activity.service';
import { HttpModule, HttpService } from '@nestjs/common';
import { of } from 'rxjs';

const mockRepository = {
  read: async (..._args) => {
    return {
      whitelist: [
        { userID: '43434343', email: 'jonas.grosch@samdock.com' },
      ],
    };
  },
};

const mockHttpService = {
  get: (..._args) => {
    return of({ data: [{ _id: 'fakeid', email: 'test' }] });
  },
  post: (..._args) => {
    return of({ data: [] });
  },
};

const mockActivityService = {
  add: async (..._args) => {
    return;
  },
};

fdescribe('Mail Webhook Controller', () => {
  let controller: MailWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      // imports: [HttpModule],
      controllers: [MailWebhookController],
      providers: [
        { provide: MailActivitySettingsRepository, useValue: mockRepository },
        { provide: ActivityService, useValue: mockActivityService },
        { provide: HttpService, useValue: mockHttpService },
      ],
    }).compile();

    controller = module.get<MailWebhookController>(MailWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should extract the correct properties from bcc mails', async () => {
    spyOn(controller, 'createFakeJWT');
    const parsed = await controller.bccMail(bccMail);
    expect(parsed.subject).toEqual('Test Subject');
    expect(parsed.from[0].address).toEqual('jonas.grosch@samdock.com');
    expect(parsed.to[0].address).toEqual('jonasgrosch96@gmail.com');
    expect(controller.createFakeJWT).toHaveBeenCalledWith('97d9380a-e151-4ee1-8418-b0401f04467e');
  });

  it('should extract the correct properties from fwd mails', async () => {
    spyOn(controller, 'createFakeJWT');
    const parsed = await controller.fwdMail(fwdMail);
    expect(parsed.subject).toEqual('Testanfrage');
    expect(parsed.from[0].address).toEqual('jonasgrosch96@gmail.com');
    expect(parsed.to[0].address).toEqual('jonas.grosch@samdock.com');
    expect(controller.createFakeJWT).toHaveBeenCalledWith('97d9380a-e151-4ee1-8418-b0401f04467e');
  });
});
