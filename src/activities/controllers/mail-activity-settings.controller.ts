import {
  Controller,
  Logger,
  Body,
  Get,
  Patch,
} from '@nestjs/common';
import { MailActivitySettingsRepository } from '../repositories/mail-activity-settings.repository';
import { GetUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';

@Controller('mail-activity-settings')
export class MailActivitySettingsController {
  protected logger = new Logger(this.constructor.name);
  constructor(private mailActivitiySettingsRepo: MailActivitySettingsRepository) { }

  @Get()
  async read(
    @GetUser() user: LoggedInUser,
  ) {
    return this.mailActivitiySettingsRepo.read(user._tenantID);
  }

  @Patch('addToWhitelist')
  async addToWhitelist(
    @Body('email') email: string,
    @GetUser() user: LoggedInUser,
  ) {
    await this.mailActivitiySettingsRepo.addWhitelistEntry(user._tenantID, email);
  }

  @Patch('removeFromWhitelist')
  async removeFromWhitelist(
    @Body('email') email: string,
    @GetUser() user: LoggedInUser,
  ) {
    await this.mailActivitiySettingsRepo.removeWhitelistEntryByMail(user._tenantID, email);
  }

}
