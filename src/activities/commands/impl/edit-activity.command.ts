import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IActivity } from '@daypaio/domain-events/activities';

export class EditActivityCommand implements ICommand {
  constructor(
    public _id: string,
    public activity: Partial<IActivity>,
    public meta: EventMetaData,
  ) {}
}
