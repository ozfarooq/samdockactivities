import { ICommand } from '@nestjs/cqrs';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { IActivity } from '@daypaio/domain-events/activities';

export class AddActivityCommand implements ICommand {
  constructor(public activity: IActivity, public meta: EventMetaData) {}
}
