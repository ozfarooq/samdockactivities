import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ActivityAggregate } from '../../models/activity.aggregate';
import { EditActivityCommand } from '../impl/edit-activity.command';
import { ActivityEditedEvent } from '@daypaio/domain-events/activities';

@CommandHandler(EditActivityCommand)
export class EditActivityCommandHandler implements ICommandHandler<EditActivityCommand> {

  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: EditActivityCommand) {

    const { _id, activity, meta } = command;

    activity.updatedAt = meta.timestamp;

    const activityAggregate = this.publisher.mergeObjectContext(
      new ActivityAggregate(activity),
    );

    const event = new ActivityEditedEvent(
      _id,
      activity,
      meta,
    );
    activityAggregate.apply(
      event,
    );

    activityAggregate.commit();
  }
}
