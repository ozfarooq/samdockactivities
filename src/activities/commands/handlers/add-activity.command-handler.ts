import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddActivityCommand } from '../impl/add-activity.command';
import { ActivityAggregate } from '../../models/activity.aggregate';
import { ActivityAddedEvent } from '@daypaio/domain-events/activities';

@CommandHandler(AddActivityCommand)
export class AddActivityCommandHandler implements ICommandHandler<AddActivityCommand> {

  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AddActivityCommand) {

    const { activity, meta } = command;

    const activityAggregate = this.publisher.mergeObjectContext(
      new ActivityAggregate(activity),
    );

    const event = new ActivityAddedEvent(
      activity,
      meta,
    );
    activityAggregate.apply(
      event,
    );

    activityAggregate.commit();
  }
}
