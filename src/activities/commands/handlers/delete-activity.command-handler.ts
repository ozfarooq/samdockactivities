import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ActivityAggregate } from '../../models/activity.aggregate';
import { DeleteActivityCommand } from '../impl/delete-activity.command';
import { ActivityDeletedEvent } from '@daypaio/domain-events/activities';

@CommandHandler(DeleteActivityCommand)
export class DeleteActivityCommandHandler implements ICommandHandler<DeleteActivityCommand> {

  constructor(
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteActivityCommand) {

    const { _id, meta } = command;

    const activityAggregate = this.publisher.mergeObjectContext(
      new ActivityAggregate({ _id }),
    );

    const event = new ActivityDeletedEvent(
      _id,
      meta,
    );
    activityAggregate.apply(
      event,
    );

    activityAggregate.commit();
  }
}
