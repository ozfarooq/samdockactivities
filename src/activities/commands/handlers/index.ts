import { AddActivityCommandHandler } from './add-activity.command-handler';
import { EditActivityCommandHandler } from './edit-activity.command-handler';
import { DeleteActivityCommandHandler } from './delete-activity.command-handler';

export const CommandHandlers = [
  AddActivityCommandHandler,
  DeleteActivityCommandHandler,
  EditActivityCommandHandler,
];
