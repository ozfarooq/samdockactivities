import { HttpModule, Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { ActivityController } from './controllers/activity.controller';
import { ActivityRepository } from './repositories/activity.repository';
import { ActivityService } from './services/activity.service';
import { QueryHandlers } from './queries/handlers';
import { CommandHandlers } from './commands/handlers';
import { EventHandlers } from './events/handlers';
import { Sagas } from './sagas';
import { Activity } from './models/activity.model';
import { ActivityGateway } from './gateways/activity.gateway';
import { MailActivitySettingsRepository } from './repositories/mail-activity-settings.repository';
import { MailWebhookController } from './controllers/mail-webhook.controller';
import { MailActivitySettings } from './models/mail-activity-settings.model';
import { MailActivitySettingsController } from './controllers/mail-activity-settings.controller';

@Module({
  imports: [
    TypegooseModule.forFeature([Activity, MailActivitySettings]),
    HttpModule,
  ],
  controllers: [
    ActivityController,
    MailWebhookController,
    MailActivitySettingsController,
  ],
  providers: [
    ActivityService,
    ActivityRepository,
    ActivityGateway,
    MailActivitySettingsRepository,
    ...QueryHandlers,
    ...CommandHandlers,
    ...EventHandlers,
    ...Sagas,
  ],
})
export class ActivityModule {}
