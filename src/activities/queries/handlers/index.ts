import { BrowseActivitiesQueryHandler } from './browse-activities.query-handler';
import { ReadActivityQueryHandler } from './read-activities.query-handler';

export const QueryHandlers = [
  BrowseActivitiesQueryHandler,
  ReadActivityQueryHandler,
];
