import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { ReadActivityQuery } from '../impl/read-activity.query';
import { Activity } from '../../models/activity.model';
import { ActivityRepository } from '../../repositories/activity.repository';

@QueryHandler(ReadActivityQuery)
export class ReadActivityQueryHandler implements IQueryHandler<ReadActivityQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: ActivityRepository) { }

  async execute(query: ReadActivityQuery): Promise<Activity> {
    const { id, meta } = query;
    try {
      return this.repo.read(id, meta);
    } catch (error) {
      this.logger.error(error.message);
      throw error;
    }
  }
}
