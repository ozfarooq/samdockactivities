import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { BrowseActivitiesQuery } from '../impl/browse-activities.query';
import { Logger } from '@nestjs/common';
import { ActivityRepository } from '../../repositories/activity.repository';
import { Activity } from '../../models/activity.model';
import { PaginationResult } from '../../../shared/bread.respository';

@QueryHandler(BrowseActivitiesQuery)
export class BrowseActivitiesQueryHandler implements IQueryHandler<BrowseActivitiesQuery> {
  private logger: Logger = new Logger(this.constructor.name);
  constructor(private repo: ActivityRepository) { }

  async execute(query: BrowseActivitiesQuery): Promise<PaginationResult<Activity> | Activity[]> {
    const { filter, limit, offset, meta } = query;
    try {
      if (!filter) {
        return this.repo.browse(limit, offset, meta);
      }
      if (filter._personID) {
        return this.repo.browseByPersons(filter._personID, limit, offset, meta);
      }
      if (filter._organizationID) {
        return this.repo.browseByOrganizations(filter._organizationID, limit, offset, meta);
      }
    } catch (error) {
      this.logger.error(error.message);
      throw error;
    }
  }
}
