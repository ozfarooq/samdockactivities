import { prop, index } from '@typegoose/typegoose';
import { plainToClassFromExist } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

class WhitelistEntry {
  @prop()
  email: string;

  @prop()
  userID: string;
}

@index({ _id: 1 })
export class MailActivitySettings {
  @ApiProperty({
    description: 'The Unique ID (same as Tenant ID)',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @prop({ type: WhitelistEntry })
  whitelist: WhitelistEntry[];

  constructor(data?: Partial<MailActivitySettings>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
