import { prop, index } from '@typegoose/typegoose';
import { plainToClassFromExist } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IActivity } from '@daypaio/domain-events/activities';

@index({ _tenantID: 1 })
@index({ _tenantID: 1, _id: 1 })
@index({ _tenantID: 1, _personID: 1 })
@index({ _tenantID: 1, _organizationID: 1 })
export class Activity implements IActivity {
  @ApiProperty({
    description: 'The related tenant',
    required: true,
  })
  @prop({ required: true })
  _tenantID: string;

  @ApiProperty({
    description: 'The ID of the activity',
    required: true,
  })
  @prop({ required: true, unique: true })
  _id: string;

  @ApiProperty({
    description: 'The related person',
    required: false,
  })
  @prop({ type: String })
  _personIDs: string[];

  @ApiProperty({
    description: 'The related organization',
    required: false,
  })
  @prop({ type: String })
  _organizationIDs: string[];

  @ApiProperty({
    description: 'The related deal',
    required: false,
  })
  @prop()
  _dealID: string;

  @ApiProperty({
    description: 'The related user',
    required: false,
  })
  @prop()
  _userID: string;

  @ApiProperty({
    description: 'Data of activity',
    required: false,
  })
  @prop()
  data: any;

  @ApiProperty({
    description: 'Type of activity',
    required: false,
  })
  @prop()
  type: string;

  @ApiProperty({
    description: 'Timestamp which used as date to show activity in activity list',
    required: false,
  })
  @prop()
  timestamp: number;

  @prop()
  createdAt: number;

  @prop()
  updatedAt: number;

  @prop()
  isLinkedEntityDeleted: boolean;

  constructor(data?: Partial<Activity>) {
    if (data) {
      plainToClassFromExist(this, data);
    }
  }
}
