import { AggregateRoot } from '@nestjs/cqrs';
import { plainToClassFromExist } from 'class-transformer';
import { IActivity } from '@daypaio/domain-events/activities';

export class ActivityAggregate extends AggregateRoot implements IActivity {
  _id: string;
  _userID: string;
  _personIDs: string[];
  _organizationIDs: string[];
  _dealID: string;
  data: any;
  type: string;
  timestamp: number;
  createdAt: number;
  updatedAt: number;

  constructor(data?: Partial<IActivity>) {
    super();

    plainToClassFromExist(this, data);
  }

}
