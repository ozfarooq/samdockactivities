import { Saga, ICommand } from '@nestjs/cqrs';
import { EMPTY, Observable } from 'rxjs';
import {
  map,
  filter,
  tap,
  groupBy,
  mergeMap,
  pairwise,
  startWith,
} from 'rxjs/operators';
import { AddActivityCommand } from '../commands/impl/add-activity.command';
import { IAggregateEvent } from 'nestjs-eventstore';
import { ActivityAggregate } from '../models/activity.aggregate';
import { nanoid } from 'nanoid';
import { DeleteActivityCommand } from '../commands/impl/delete-activity.command';
import {
  AssignedToDealAsContactPersonEvent,
  AssignedToLeadAsContactPersonEvent,
  DealRelatedToPersonEvent,
  DealUnrelatedFromPersonEvent,
  LeadRelatedToPersonEvent,
  LeadUnrelatedFromPersonEvent,
  OrganizationRelatedToPersonEvent,
  OrganizationUnrelatedFromPersonEvent,
  PersonRelatedLeadQualifiedEvent,
  PersonRelatedLeadUnqualifiedEvent,
  PersonRelatedTaskCompletedEvent,
  PersonRelatedTaskReopenedEvent,
  RemovedFromDealAsContactPersonEvent,
  RemovedFromLeadAsContactPersonEvent,
  TaskLinkedToPersonEvent,
  TaskUnlinkedFromPersonEvent,
} from '@daypaio/domain-events/persons';

type ReversalPairs = {
  [type: string]: { reversalType: string, condition: (previous: any, current: any) => boolean },
};

const reversalPairs: ReversalPairs = {
  PersonRelatedTaskCompletedEvent: {
    reversalType: 'PersonRelatedTaskReopenedEvent',
    condition: (
      previous: PersonRelatedTaskCompletedEvent,
      current: PersonRelatedTaskReopenedEvent,
    ) => previous._taskID === current._taskID,
  },
  PersonRelatedTaskReopenedEvent: {
    reversalType: 'PersonRelatedTaskCompletedEvent',
    condition: (
      previous:   PersonRelatedTaskReopenedEvent,
      current: PersonRelatedTaskCompletedEvent,
    ) => previous._taskID === current._taskID,
  },
  TaskLinkedToPersonEvent: {
    reversalType: 'TaskUnlinkedFromPersonEvent',
    condition: (
      previous: TaskLinkedToPersonEvent,
      current: TaskUnlinkedFromPersonEvent,
    ) => previous._taskID === current._taskID,
  },
  TaskUnlinkedFromPersonEvent: {
    reversalType: 'TaskLinkedToPersonEvent',
    condition: (
      previous: TaskUnlinkedFromPersonEvent,
      current: TaskLinkedToPersonEvent,
    ) => previous._taskID === current._taskID,
  },
  DealRelatedToPersonEvent: {
    reversalType: 'DealUnrelatedFromPersonEvent',
    condition: (
      previous: DealRelatedToPersonEvent,
      current: DealUnrelatedFromPersonEvent,
    ) => previous._dealID === current._dealID,
  },
  DealUnrelatedFromPersonEvent: {
    reversalType: 'DealRelatedToPersonEvent',
    condition: (
      previous: DealUnrelatedFromPersonEvent,
      current: DealRelatedToPersonEvent,
    ) => previous._dealID === current._dealID,
  },
  LeadRelatedToPersonEvent: {
    reversalType: 'LeadUnrelatedFromPersonEvent',
    condition: (
      previous: LeadRelatedToPersonEvent,
      current: LeadUnrelatedFromPersonEvent,
    ) => previous._leadID === current._leadID,
  },
  LeadUnrelatedFromPersonEvent: {
    reversalType: 'LeadRelatedToPersonEvent',
    condition: (
      previous: LeadUnrelatedFromPersonEvent,
      current: LeadRelatedToPersonEvent,
    ) => previous._leadID === current._leadID,
  },
  OrganizationRelatedToPersonEvent: {
    reversalType: 'OrganizationUnrelatedFromPersonEvent',
    condition: (
      previous: OrganizationRelatedToPersonEvent,
      current: OrganizationUnrelatedFromPersonEvent,
    ) => previous._organizationID === current._organizationID,
  },
  OrganizationUnrelatedFromPersonEvent: {
    reversalType: 'OrganizationRelatedToPersonEvent',
    condition: (
      previous: OrganizationUnrelatedFromPersonEvent,
      current: OrganizationRelatedToPersonEvent,
    ) => previous._organizationID === current._organizationID,
  },
  AssignedToDealAsContactPersonEvent: {
    reversalType: 'RemovedFromDealAsContactPersonEvent',
    condition: (
      previous: AssignedToDealAsContactPersonEvent,
      current: RemovedFromDealAsContactPersonEvent,
    ) => previous._dealID === current._dealID,
  },
  RemovedFromDealAsContactPersonEvent: {
    reversalType: 'AssignedToDealAsContactPersonEvent',
    condition: (
      previous: RemovedFromDealAsContactPersonEvent,
      current: AssignedToDealAsContactPersonEvent,
    ) => previous._dealID === current._dealID,
  },
  AssignedToLeadAsContactPersonEvent: {
    reversalType: 'RemovedFromLeadAsContactPersonEvent',
    condition: (
      previous: AssignedToLeadAsContactPersonEvent,
      current: RemovedFromLeadAsContactPersonEvent,
    ) => previous._dealID === current._dealID,
  },
  RemovedFromLeadAsContactPersonEvent: {
    reversalType: 'AssignedToLeadAsContactPersonEvent',
    condition: (
      previous: RemovedFromLeadAsContactPersonEvent,
      current: AssignedToLeadAsContactPersonEvent,
    ) => previous._dealID === current._dealID,
  },
  PersonRelatedLeadQualifiedEvent: {
    reversalType: 'PersonRelatedLeadUnqualifiedEvent',
    condition: (
      previous: PersonRelatedLeadQualifiedEvent,
      current: PersonRelatedLeadUnqualifiedEvent,
    ) => previous._leadID === current._leadID,
  },
  PersonRelatedLeadUnqualifiedEvent: {
    reversalType: 'PersonRelatedLeadQualifiedEvent',
    condition: (
      previous: PersonRelatedLeadUnqualifiedEvent,
      current: PersonRelatedLeadQualifiedEvent,
    ) => previous._leadID === current._leadID,
  },
};

export class PersonActivitySaga {
  @Saga()
  systemActivity = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      filter((event: IAggregateEvent) => {
        return event.streamName.startsWith('persons');
      }),
      groupBy(
        (event: any) =>
          `${event.meta?._userID}:${event.streamName.split('persons-')[1]}`,
      ),
      mergeMap(group =>
        group.pipe(
          startWith(EMPTY),
          tap(event => event.__resultingID = nanoid()),
          pairwise(),
          map((events) => {
            const previous = events[0];
            const current = events[1];

            const reversal = reversalPairs[previous.constructor.name];

            if (
              !previous.__wasSkipped &&
              reversal?.reversalType === current.constructor.name &&
              reversal?.condition(previous, current) &&
              current.meta?.timestamp - previous.meta?.timestamp < 60000
            ) {
              current.__wasSkipped = true;

              return new DeleteActivityCommand(
                previous.__resultingID,
                current.meta,
              );
            }

            const _personID = current.streamName.split('persons-')[1];

            const activity = new ActivityAggregate({
              _personIDs: [_personID],
              _userID: current.meta?._userID,
              _id: current.__resultingID,
              type: current.constructor.name,
              data: current,
              timestamp: current.meta?.timestamp,
              createdAt: current.meta?.timestamp,
            });

            return new AddActivityCommand(activity, current.meta);
          }),
        ),
      ),
    );
  }
}
