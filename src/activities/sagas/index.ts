import { PersonActivitySaga } from './person-activity.saga';
import { OrganizationActivitySaga } from './organization-activity.saga';

export const Sagas = [
  PersonActivitySaga,
  OrganizationActivitySaga,
];
