import { Test, TestingModule } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventBus } from '@nestjs/cqrs';
import { PersonActivitySaga } from './person-activity.saga';
import { PersonRelatedTaskCompletedEvent, PersonRelatedTaskReopenedEvent } from '@daypaio/domain-events/persons';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { AddActivityCommandHandler } from '../commands/handlers/add-activity.command-handler';
import { DeleteActivityCommandHandler } from '../commands/handlers/delete-activity.command-handler';

const wait = (ms: number) => {
  return new Promise(resolve => setTimeout(res => resolve(res), ms));
};

fdescribe('ActivityService', () => {
  let saga: PersonActivitySaga;
  let eventBus: EventBus;
  let commandBus: CommandBus;
  let addActivityCommandHandler: AddActivityCommandHandler;
  let deleteActivityCommandHandler: DeleteActivityCommandHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        PersonActivitySaga,
        AddActivityCommandHandler,
        DeleteActivityCommandHandler,
      ],
    }).compile();

    saga = module.get<PersonActivitySaga>(PersonActivitySaga);
    eventBus = module.get<EventBus>(EventBus);
    commandBus = module.get<CommandBus>(CommandBus);
    addActivityCommandHandler = module.get(AddActivityCommandHandler);
    deleteActivityCommandHandler = module.get(DeleteActivityCommandHandler);
    commandBus.register([AddActivityCommandHandler, DeleteActivityCommandHandler]);
    eventBus.registerSagas([PersonActivitySaga]);
  });

  it('should be defined', async () => {
    spyOn(addActivityCommandHandler, 'execute');
    spyOn(deleteActivityCommandHandler, 'execute');

    expect(saga).toBeDefined();

    const event1 = new PersonRelatedTaskCompletedEvent('0', '0', new EventMetaData('0', '0'));
    eventBus.publish(event1);
    expect(addActivityCommandHandler.execute).toBeCalledTimes(1);
    await wait(200);
    const event2 = new PersonRelatedTaskReopenedEvent('0', '0', new EventMetaData('0', '0'));
    eventBus.publish(event2);
    expect(deleteActivityCommandHandler.execute).toBeCalledTimes(1);
    await wait(200);
    eventBus.publish(event1);
    expect(addActivityCommandHandler.execute).toBeCalledTimes(2);
  });
});
