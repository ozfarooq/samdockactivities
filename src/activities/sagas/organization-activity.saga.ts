import { Saga, ICommand } from '@nestjs/cqrs';
import { EMPTY, Observable } from 'rxjs';
import {
  map,
  filter,
  tap,
  groupBy,
  mergeMap,
  pairwise,
  startWith,
} from 'rxjs/operators';
import { AddActivityCommand } from '../commands/impl/add-activity.command';
import { IAggregateEvent } from 'nestjs-eventstore';
import { ActivityAggregate } from '../models/activity.aggregate';
import { nanoid } from 'nanoid';
import { DeleteActivityCommand } from '../commands/impl/delete-activity.command';
import {
  DealRelatedToOrganizationEvent,
  DealUnrelatedFromOrganizationEvent,
  LeadRelatedToOrganizationEvent,
  LeadUnrelatedFromOrganizationEvent,
  OrganizationRelatedLeadQualifiedEvent,
  OrganizationRelatedLeadUnqualifiedEvent,
  OrganizationRelatedTaskCompletedEvent,
  OrganizationRelatedTaskReopenedEvent,
  PersonRelatedToOrganizationEvent,
  PersonUnrelatedFromOrganizationEvent,
  TaskLinkedToOrganizationEvent,
  TaskUnlinkedFromOrganizationEvent,
} from '@daypaio/domain-events/organizations';

type ReversalPairs = {
  [type: string]: { reversalType: string, condition: (previous: any, current: any) => boolean },
};

const reversalPairs: ReversalPairs = {
  OrganizationRelatedTaskCompletedEvent: {
    reversalType: 'OrganizationRelatedTaskReopenedEvent',
    condition: (
      previous: OrganizationRelatedTaskCompletedEvent,
      current: OrganizationRelatedTaskReopenedEvent,
    ) => previous._taskID === current._taskID,
  },
  OrganizationRelatedTaskReopenedEvent: {
    reversalType: 'OrganizationRelatedTaskCompletedEvent',
    condition: (
      previous: OrganizationRelatedTaskReopenedEvent,
      current: OrganizationRelatedTaskCompletedEvent,
    ) => previous._taskID === current._taskID,
  },
  TaskLinkedToOrganizationEvent: {
    reversalType: 'TaskUnlinkedFromOrganizationEvent',
    condition: (
      previous: TaskLinkedToOrganizationEvent,
      current: TaskUnlinkedFromOrganizationEvent,
    ) => previous._taskID === current._taskID,
  },
  TaskUnlinkedFromOrganizationEvent: {
    reversalType: 'TaskLinkedToOrganizationEvent',
    condition: (
      previous: TaskUnlinkedFromOrganizationEvent,
      current: TaskLinkedToOrganizationEvent,
    ) => previous._taskID === current._taskID,
  },
  DealRelatedToOrganizationEvent: {
    reversalType: 'DealUnrelatedFromOrganizationEvent',
    condition: (
      previous: DealRelatedToOrganizationEvent,
      current: DealUnrelatedFromOrganizationEvent,
    ) => previous._dealID === current._dealID,
  },
  DealUnrelatedFromOrganizationEvent: {
    reversalType: 'DealRelatedToOrganizationEvent',
    condition: (
      previous: DealUnrelatedFromOrganizationEvent,
      current: DealRelatedToOrganizationEvent,
    ) => previous._dealID === current._dealID,
  },
  LeadRelatedToOrganizationEvent: {
    reversalType: 'LeadUnrelatedFromOrganizationEvent',
    condition: (
      previous: LeadRelatedToOrganizationEvent,
      current: LeadUnrelatedFromOrganizationEvent,
    ) => previous._leadID === current._leadID,
  },
  LeadUnrelatedFromOrganizationEvent: {
    reversalType: 'LeadRelatedToOrganizationEvent',
    condition: (
      previous: LeadUnrelatedFromOrganizationEvent,
      current: LeadRelatedToOrganizationEvent,
    ) => previous._leadID === current._leadID,
  },
  OrganizationRelatedLeadQualifiedEvent: {
    reversalType: 'OrganizationRelatedLeadUnqualifiedEvent',
    condition: (
      previous: OrganizationRelatedLeadQualifiedEvent,
      current: OrganizationRelatedLeadUnqualifiedEvent,
    ) => previous._leadID === current._leadID,
  },
  OrganizationRelatedLeadUnqualifiedEvent: {
    reversalType: 'OrganizationRelatedLeadQualifiedEvent',
    condition: (
      previous: OrganizationRelatedLeadUnqualifiedEvent,
      current: OrganizationRelatedLeadQualifiedEvent,
    ) => previous._leadID === current._leadID,
  },
  PersonRelatedToOrganizationEvent: {
    reversalType: 'PersonUnrelatedFromOrganizationEvent',
    condition: (
      previous: PersonRelatedToOrganizationEvent,
      current: PersonUnrelatedFromOrganizationEvent,
    ) => previous._personID === current._personID,
  },
  PersonUnrelatedFromOrganizationEvent: {
    reversalType: 'PersonRelatedToOrganizationEvent',
    condition: (
      previous: PersonUnrelatedFromOrganizationEvent,
      current: PersonRelatedToOrganizationEvent,
    ) => previous._personID === current._personID,
  },
};

export class OrganizationActivitySaga {
  @Saga()
  systemActivity = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      filter((event: IAggregateEvent) => {
        return event.streamName.startsWith('organizations');
      }),
      groupBy(
        (event: any) =>
          `${event.meta?._userID}:${event.streamName.split('organizations-')[1]}`,
      ),
      mergeMap(group =>
        group.pipe(
          startWith(EMPTY),
          tap(event => event.__resultingID = nanoid()),
          pairwise(),
          map((events) => {
            const previous = events[0];
            const current = events[1];

            const reversal = reversalPairs[previous.constructor.name];

            if (
              !previous.__wasSkipped &&
              reversal?.reversalType === current.constructor.name &&
              reversal?.condition(previous, current) &&
              current.meta?.timestamp - previous.meta?.timestamp < 60000
            ) {
              current.__wasSkipped = true;

              return new DeleteActivityCommand(
                previous.__resultingID,
                current.meta,
              );
            }

            const _organizationID = current.streamName.split('organizations-')[1];

            const activity = new ActivityAggregate({
              _organizationIDs: [_organizationID],
              _userID: current.meta?._userID,
              _id: current.__resultingID,
              type: current.constructor.name,
              data: current,
              timestamp: current.meta?.timestamp,
              createdAt: current.meta?.timestamp,
            });

            return new AddActivityCommand(activity, current.meta);
          }),
        ),
      ),
    );
  }
}
