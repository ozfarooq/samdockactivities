import {
  SubscribeMessage,
  WebSocketGateway,
  MessageBody,
  ConnectedSocket,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { GetWsUser, LoggedInUser } from '../../shared/decorators/get-user.decorator';
import { Socket } from 'socket.io';
import { EventBus } from '@nestjs/cqrs';
import { filter, map, startWith } from 'rxjs/operators';
import { ActivityService } from '../services/activity.service';
import { IActivity, ActivityAddedEvent, ActivityEditedEvent, ActivityDeletedEvent } from '@daypaio/domain-events/activities';
import { PersonDeletedEvent } from '@daypaio/domain-events/persons';
import { OrganizationDeletedEvent } from '@daypaio/domain-events/organizations';
import { DealDeletedEvent } from '@daypaio/domain-events/deals';
import { TaskDeletedEvent } from '@daypaio/domain-events/tasks';
import { of } from 'rxjs';

@WebSocketGateway({ path: '/api/activities/socket.io' })
export class ActivityGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: any;

  protected logger = new Logger(this.constructor.name);

  public connectedSockets: Socket[] = [];

  constructor(
    private eventBus: EventBus,
    private service: ActivityService,
  ) {
    this.logger.log('Web socket is listening');
  }

  async handleConnection(client: any) {
    try {
      const hasConnect =
        this.connectedSockets.findIndex(p => p.id === client.id) > -1;
      if (hasConnect) {
        this.connectedSockets = this.connectedSockets.filter(
          p => p.id !== client.id,
        );
      }

      console.log('Connected:', client.id);
      this.logger.log(`Connected: ${client.id}`);

      this.connectedSockets.push(client);
    } catch (error) {
      this.logger.log(`Cant handle connection: ${error}`);
    }
  }

  async handleDisconnect(@ConnectedSocket() client: Socket) {
    for (let i = 0; i < this.connectedSockets.length; i += 1) {
      if (this.connectedSockets[i] === client) {
        this.connectedSockets.splice(i, 1);
        break;
      }
    }
    console.log('Disconnected:', client.id);
    this.logger.log(`Disconnected: ${client.id}`);
  }

  @SubscribeMessage('activities-by-person')
  async subscribeByPerson(
    @MessageBody() message: { _personID: string },
    @GetWsUser() user: LoggedInUser,
  ) {
    this.logger.log('Subscribed to activities-by-person');
    const activities = await this.service.browseByPerson(
      message._personID,
      0,
      0,
      user.generateMeta(),
    );

    return of({ event: 'activities-by-person', data: activities });
  }

  @SubscribeMessage('activities-by-organization')
  async subscribeByOrganization(
    @MessageBody() message: { _organizationID: string },
    @GetWsUser() user: LoggedInUser,
  ) {
    this.logger.log('Subscribed to activities-by-organization');
    const activities = await this.service.browseByOrganization(
      message._organizationID,
      0,
      0,
      user.generateMeta(),
    );

    return of({ event: 'activities-by-organization', data: activities });
  }

  @SubscribeMessage('activities')
  async subscribe(
    @GetWsUser() user: LoggedInUser,
  ) {
    this.logger.log('Subscribed to activities');
    const activities = await this.service.browse(0, 0, user.generateMeta());

    return this.createEventPipeline(activities, user._tenantID);
  }

  createEventPipeline(activities: IActivity[], _tenantID: string) {
    const event = 'activities';
    const events = [
      ActivityAddedEvent,
      ActivityEditedEvent,
      ActivityDeletedEvent,
      PersonDeletedEvent,
      OrganizationDeletedEvent,
      DealDeletedEvent,
      TaskDeletedEvent,
    ] as const;
    type EventType = ActivityAddedEvent | ActivityEditedEvent | ActivityDeletedEvent |
      PersonDeletedEvent | OrganizationDeletedEvent | DealDeletedEvent | TaskDeletedEvent;

    return this.eventBus.pipe(
      filter((event: any) => {
        return events.some(eventInst => event instanceof eventInst);
      }),
      filter((event: EventType) => {
        return event.meta._tenantID === _tenantID;
      }),
      map((data: EventType) => {
        return { event, data: { data, type: data.constructor.name } };
      }),
      startWith(activities),
    );
  }

}
