import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddActivityCommand } from '../commands/impl/add-activity.command';
import { EventMetaData } from '@daypaio/domain-events/shared';
import { BreadService } from '../../shared/services/bread.service';
import { BrowseActivitiesQuery } from '../queries/impl/browse-activities.query';
import { ReadActivityQuery } from '../queries/impl/read-activity.query';
import { EditActivityCommand } from '../commands/impl/edit-activity.command';
import { DeleteActivityCommand } from '../commands/impl/delete-activity.command';
import { IActivity } from '@daypaio/domain-events/activities';

@Injectable()
export class ActivityService extends BreadService<IActivity> {
  constructor(commandBus: CommandBus, queryBus: QueryBus) {
    super(commandBus, queryBus);
  }

  browse(limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(new BrowseActivitiesQuery(null, limit, offset, meta));
  }

  browseByPerson(personID: string, limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(
      new BrowseActivitiesQuery({ _personID: personID }, limit, offset, meta),
    );
  }

  browseByOrganization(organizationID: string, limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(
      new BrowseActivitiesQuery({ _organizationID: organizationID }, limit, offset, meta),
    );
  }

  browseByTask(taskID: string, limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(
      new BrowseActivitiesQuery({ _taskID: taskID }, limit, offset, meta),
    );
  }

  browseByDeal(dealID: string, limit: number, offset: number, meta: EventMetaData) {
    return this.executeQuery(
      new BrowseActivitiesQuery({ _dealID: dealID }, limit, offset, meta),
    );
  }

  read(id: string, meta: EventMetaData) {
    return this.executeQuery(new ReadActivityQuery(id, meta));
  }

  edit(id: string, activity: Partial<IActivity>, meta: EventMetaData) {
    return this.executeCommand(new EditActivityCommand(id, activity, meta));
  }

  add(activity: IActivity, meta: EventMetaData) {
    return this.executeCommand(new AddActivityCommand(activity, meta));
  }

  delete(id: string, meta: EventMetaData) {
    return this.executeCommand(new DeleteActivityCommand(id, meta));
  }

}
