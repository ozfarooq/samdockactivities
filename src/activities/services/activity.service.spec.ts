import { Test, TestingModule } from '@nestjs/testing';
import { ActivityService } from './activity.service';
import { getModelToken } from 'nestjs-typegoose';
import { CqrsModule } from '@nestjs/cqrs';

const mockPersonModel = () => ({});

describe('ActivityService', () => {
  let service: ActivityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ActivityService,
        {
          provide: getModelToken('Person'),
          useFactory: mockPersonModel,
        },
      ],
    }).compile();

    service = module.get<ActivityService>(ActivityService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
