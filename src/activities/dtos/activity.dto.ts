import { IActivity } from '@daypaio/domain-events/activities';
import { plainToClassFromExist } from 'class-transformer';
import {
  IsString,
  IsOptional,
  IsNumber,
  IsArray,
} from 'class-validator';

export class ActivityDTO implements IActivity {

  constructor(data?: Partial<ActivityDTO>) {
    plainToClassFromExist(this, data);
  }

  @IsString()
  _id: string;

  @IsString()
  _userID: string;

  @IsArray()
  @IsOptional()
  _personIDs: string[];

  @IsArray()
  @IsOptional()
  _organizationIDs: string[];

  @IsString()
  @IsOptional()
  _dealID: string;

  @IsOptional()
  data: any;

  @IsString()
  @IsOptional()
  type: string;

  @IsNumber()
  @IsOptional()
  timestamp: number;

  @IsNumber()
  @IsOptional()
  createdAt: number;

  @IsNumber()
  @IsOptional()
  updatedAt: number;

}
