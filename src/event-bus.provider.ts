import {
  EventStoreBusConfig,
  EventStoreSubscriptionType,
} from 'nestjs-eventstore';
import * as TenantEventInstantiators  from '@daypaio/domain-events/tenants/impl';
import * as PersonEventInstantiators  from '@daypaio/domain-events/persons/impl';
import * as OrganizationEventInstantiators from '@daypaio/domain-events/organizations/impl';
import * as ActivityEventInstantiators from '@daypaio/domain-events/activities/impl';
import * as TaskEventInstantiators from '@daypaio/domain-events/tasks/impl';
import * as DealEventInstantiators from '@daypaio/domain-events/deals/impl';
import * as UserEventInstantiators from '@daypaio/domain-events/users/impl';

export const eventStoreBusConfig: EventStoreBusConfig = {
  subscriptions: [
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-persons',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-organizations',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-activities',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-users',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$ce-tasks',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-PersonDeletedEvent',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-OrganizationDeletedEvent',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-TaskDeletedEvent',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-DealDeletedEvent',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-LeadDeletedEvent',
      persistentSubscriptionName: 'activities',
    },
    {
      type: EventStoreSubscriptionType.Persistent,
      stream: '$et-TenantDeletedEvent',
      persistentSubscriptionName: 'activities',
    },
  ],
  events: {
    ...PersonEventInstantiators,
    ...TenantEventInstantiators,
    ...OrganizationEventInstantiators,
    ...ActivityEventInstantiators,
    ...TaskEventInstantiators,
    ...DealEventInstantiators,
    ...UserEventInstantiators,
  },
};
