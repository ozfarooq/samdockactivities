import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { Activity } from '../activities/models/activity.model';

export class MultipleOrgLinking1611916242507 implements MigrationInterface {

  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  public async up(): Promise<void> {

    try {
      setupDotenv();

      mongoose.connect(process.env.MONGO_CONNECTION_STRING);
      mongoose.set('debug', true);
      this.model = getModelForClass(Activity);
      this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);

      await this.model.updateMany(
        {},
        [
          {
            $set: { _organizationIDs: ['$_organizationID'] },
          },
          {
            $unset: ['_organizationID'],
          },
        ],
      );

    } catch (err) {
      this.logger.error('Error is handled!');
      this.logger.error(err);
    }
  }

  public async down(): Promise<void> {
  }

}
