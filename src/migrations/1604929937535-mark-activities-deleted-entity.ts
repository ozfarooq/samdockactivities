import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { Activity } from '../activities/models/activity.model';
import * as esClient from 'node-eventstore-client';

export class MarkActivitiesDeletedEntity1604929937535 implements MigrationInterface {

  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  esConnection: esClient.EventStoreNodeConnection;

  public async up(): Promise<void> {

    try {
      setupDotenv();

      mongoose.connect(process.env.MONGO_CONNECTION_STRING);
      mongoose.set('debug', true);
      this.model = getModelForClass(Activity);
      this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);

      const connectionSettings = {
        defaultUserCredentials: {
          username: process.env.EVENT_STORE_USERNAME,
          password: process.env.EVENT_STORE_PASSWORD,
        },
      };

      const tcpEndpoint = {
        host: process.env.EVENT_STORE_TCP_HOST || 'localhost',
        port: parseInt(process.env.EVENT_STORE_TCP_PORT, 10) || 1113,
      };

      this.esConnection = esClient.createConnection(connectionSettings, tcpEndpoint);

      await this.esConnection.connect();

      const eventGroups = await Promise.all([
        this.readEvents('$et-PersonDeletedEvent'),
        this.readEvents('$et-OrganizationDeletedEvent'),
        this.readEvents('$et-TaskDeletedEvent'),
        this.readEvents('$et-DealDeletedEvent'),
      ]);

      const events = [...eventGroups[0], ...eventGroups[1], ...eventGroups[2], ...eventGroups[3]];

      this.logger.log(`${events.length} deleting events found`);

      const foundDeletedIDs = { persons: [], organizations: [], tasks: [], deals: [] };

      for (const event of events) {
        if (!event.originalEvent?.eventStreamId || !event.originalEvent?.data) {
          this.logger.log(event);
          continue;
        }

        const _id = event.originalEvent?.data?.toString().split('-').slice(1).join('-');

        if (event.originalEvent?.eventStreamId === '$et-PersonDeletedEvent') {
          foundDeletedIDs.persons.push(_id);
        } else if (event.originalEvent?.eventStreamId === '$et-OrganizationDeletedEvent') {
          foundDeletedIDs.organizations.push(_id);
        } else if (event.originalEvent?.eventStreamId === '$et-TaskDeletedEvent') {
          foundDeletedIDs.tasks.push(_id);
        } else if (event.originalEvent?.eventStreamId === '$et-DealDeletedEvent') {
          foundDeletedIDs.deals.push(_id);
        }
      }

      this.logger.log(`${foundDeletedIDs.persons.length} deleted persons found`);
      this.logger.log(`${foundDeletedIDs.organizations.length} deleted organizations found`);
      this.logger.log(`${foundDeletedIDs.tasks.length} deleted tasks found`);
      this.logger.log(`${foundDeletedIDs.deals.length} deleted deals found`);
      if (foundDeletedIDs.persons.length) {
        await this.model.updateMany(
          { 'data._personIDs': { $in: foundDeletedIDs.persons } },
          { $set: { isLinkedEntityDeleted: true } },
        );
      }
      if (foundDeletedIDs.organizations.length) {
        await this.model.updateMany(
          { 'data._organizationID': { $in: foundDeletedIDs.organizations } },
          { $set: { isLinkedEntityDeleted: true } },
        );
      }

      if (foundDeletedIDs.tasks.length) {
        await this.model.updateMany(
          { 'data._taskID': { $in: foundDeletedIDs.tasks } },
          { $set: { isLinkedEntityDeleted: true } },
        );
      }

      if (foundDeletedIDs.deals.length) {
        await this.model.updateMany(
          { 'data._dealID': { $in: foundDeletedIDs.deals } },
          { $set: { isLinkedEntityDeleted: true } },
        );
      }

      this.logger.log('All deleted events were handled');
    } catch (err) {
      this.logger.error('Error is handled!');
      this.logger.error(err);
    }
  }

  readEvents(streamName: string): Promise<esClient.ResolvedEvent[]> {
    const events: esClient.ResolvedEvent[] = [];

    return new Promise(
      (resolve) => {
        this.esConnection.subscribeToStreamFrom(
          streamName,
          null,
          true,
          (_sub, event) => {
            events.push(event);
          },
          () => resolve(events),
        );
      },
    );
  }

  public async down(): Promise<void> {
  }

}
