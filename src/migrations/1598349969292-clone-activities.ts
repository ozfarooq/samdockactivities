import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { Activity } from '../activities/models/activity.model';
import * as esClient from 'node-eventstore-client';
import { nanoid } from 'nanoid';

export class CloneActivities1598349969292 implements MigrationInterface {

  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  esConnection: esClient.EventStoreNodeConnection;

  public async up(): Promise<void> {
    setupDotenv();

    mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    mongoose.set('debug', true);
    this.model = getModelForClass(Activity);
    this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);

    const connectionSettings = {
      defaultUserCredentials: {
        username: process.env.EVENT_STORE_USERNAME,
        password: process.env.EVENT_STORE_PASSWORD,
      },
    };

    const tcpEndpoint = {
      host: process.env.EVENT_STORE_TCP_HOST || 'localhost',
      port: parseInt(process.env.EVENT_STORE_TCP_PORT, 10) || 1113,
    };

    this.esConnection = esClient.createConnection(connectionSettings, tcpEndpoint);

    await this.esConnection.connect();

    const eventGroups = await Promise.all([
      this.readEvents('$ce-persons'),
      this.readEvents('$ce-organizations'),
    ]);

    const events = [...eventGroups[0], ...eventGroups[1]];

    const mappedEvents = [];

    for (const event of events) {
      const regex = new RegExp('^(persons|organizations)-[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$');

      if (!event.event?.eventStreamId || !regex.test(event.event.eventStreamId)) {
        continue;
      }

      const _personID = event.event.eventStreamId.startsWith('persons-') ?
        event.event.eventStreamId.split('persons-')[1] :
        undefined;

      const _organizationID = event.event.eventStreamId.startsWith('organizations-') ?
        event.event.eventStreamId.split('organizations-')[1] :
        undefined;

      const data = JSON.parse(event.event.data.toString());

      if (!data?.meta?._tenantID) {
        continue;
      }

      const activity = new Activity({
        data,
        _organizationIDs: [_organizationID],
        _id: nanoid(),
        _personIDs: [_personID],
        _tenantID: data.meta?._tenantID,
        _userID: data.meta?._userID,
        type: event.event.eventType,
        timestamp: data.meta?.timestamp,
        createdAt: data.meta?.timestamp,
      });

      if (!activity) {
        continue;
      }

      mappedEvents.push(activity);
    }

    try {
      await this.model.insertMany(mappedEvents);
    } catch (error) {
      this.logger.error(error);
    }
  }

  readEvents(streamName: string): Promise<esClient.ResolvedEvent[]> {
    const events: esClient.ResolvedEvent[] = [];

    return new Promise(
      (resolve) => {
        this.esConnection.subscribeToStreamFrom(
          streamName,
          null,
          true,
          (_sub, event) => {
            events.push(event);
          },
          () => resolve(events),
        );
      },
    );
  }

  public async down(): Promise<void> {
  }

}
