import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import { Activity } from '../activities/models/activity.model';

export class DealEventRefactoring1625490612756 implements MigrationInterface {

  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  public async up(): Promise<void> {

    try {
      setupDotenv();

      mongoose.connect(process.env.MONGO_CONNECTION_STRING);
      mongoose.set('debug', true);
      this.model = getModelForClass(Activity);
      this.logger = new Logger(`${this.model.modelName} ${this.constructor.name}`);

      await this.model.updateMany(
        { type: 'OrganizationRelatedDealStatusChangedEvent' },
        { $set: { type: 'OrganizationRelatedDealStageChangedEvent' } },
      );

      await this.model.updateMany(
        { type: 'PersonRelatedDealStatusChangedEvent' },
        { $set: { type: 'PersonRelatedDealStageChangedEvent' } },
      );

      await this.model.updateMany(
        { type: 'RelatedDealStatusChangedEvent' },
        { $set: { type: 'RelatedDealStageChangedEvent' } },
      );

    } catch (err) {
      this.logger.error('Error is handled!');
      this.logger.error(err);
    }
  }

  public async down(): Promise<void> {
  }

}
