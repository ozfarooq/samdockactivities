import { MigrationInterface } from 'typeorm';
import * as mongoose from 'mongoose';
import { setupDotenv } from '../dotenv.setup';
import { Logger } from '@nestjs/common';
import { getModelForClass } from '@typegoose/typegoose';
import * as esClient from 'node-eventstore-client';
import {
  UserAddedEvent,
  UserDeletedEvent,
  UserEditedEvent,
} from '@daypaio/domain-events/users';
import { MailActivitySettings } from '../activities/models/mail-activity-settings.model';

export class MailLoggingWhitelist1607688973604 implements MigrationInterface {
  private model: mongoose.Model<InstanceType<any>>;
  private logger: Logger;

  esConnection: esClient.EventStoreNodeConnection;

  public async up(): Promise<void> {
    try {
      setupDotenv();

      mongoose.connect(process.env.MONGO_CONNECTION_STRING);
      mongoose.set('debug', true);
      this.model = getModelForClass(MailActivitySettings);
      this.logger = new Logger(
        `${this.model.modelName} ${this.constructor.name}`,
      );

      const connectionSettings = {
        defaultUserCredentials: {
          username: process.env.EVENT_STORE_USERNAME,
          password: process.env.EVENT_STORE_PASSWORD,
        },
      };

      const tcpEndpoint = {
        host: process.env.EVENT_STORE_TCP_HOST || 'localhost',
        port: parseInt(process.env.EVENT_STORE_TCP_PORT, 10) || 1113,
      };

      this.esConnection = esClient.createConnection(
        connectionSettings,
        tcpEndpoint,
      );

      await this.esConnection.connect();

      const streams = await Promise.all([
        this.readEvents('$et-UserAddedEvent'),
        this.readEvents('$et-UserEditedEvent'),
        this.readEvents('$et-UserDeletedEvent'),
      ]);

      const events = [...streams[0], ...streams[1], ...streams[2]].sort((a, b) => {
        return a.event?.created?.getTime() - b.event?.created?.getTime();
      });

      this.logger.log(`${events.length} deleting events found`);

      const userMap: Map<
        string,
        { userID: string; email: string }[]
      > = new Map();

      const eventHandlers = {
        UserAddedEvent,
        UserDeletedEvent,
        UserEditedEvent,
      };

      for (const event of events) {
        if (!event.originalEvent?.eventStreamId || !event.originalEvent?.data) {
          this.logger.log(event);
          continue;
        }

        if (
          (event.link !== null && !event.isResolved) ||
          !event.event ||
          !event.event.isJson
        ) {
          this.logger.error('Received event that could not be resolved!');
          continue;
        }
        const handler = eventHandlers[event.event.eventType];
        if (!handler) {
          this.logger.error('Received event that could not be handled!');
          continue;
        }
        const data = Object.values(JSON.parse(event.event.data.toString()));

        const parsed:
          | UserAddedEvent
          | UserDeletedEvent
          | UserEditedEvent = new eventHandlers[event.event.eventType](...data);

        if (!parsed.meta?._tenantID) {
          continue;
        }

        if (parsed instanceof UserAddedEvent && parsed.meta._tenantID) {
          userMap.set(parsed.meta._tenantID, [
            ...(userMap.get(parsed.meta._tenantID) || []),
            { userID: parsed._id, email: parsed.user.email },
          ]);
        } else if (
          parsed instanceof UserEditedEvent && parsed.user.email
        ) {
          userMap.set(
            parsed.meta._tenantID,
            (userMap.get(parsed.meta._tenantID) || []).map(
              entry => entry.userID === parsed._id ? { ...entry, email: parsed.user.email } : entry,
            ),
          );
        } else if (
          parsed instanceof UserDeletedEvent
        ) {
          userMap.set(
            parsed.meta._tenantID,
            (userMap.get(parsed.meta._tenantID) || []).filter(
              entry => entry.userID !== parsed._id,
            ),
          );
        }
      }

      await this.model.insertMany(
        [...userMap.entries()].map(([key, value]) => ({
          _id: key,
          whitelist: value,
        })).filter(entry => entry.whitelist?.length > 0),
      );

      this.logger.log('All settings were created');
    } catch (err) {
      this.logger.error('Error is handled!');
      this.logger.error(err);
      throw err;
    }
  }

  readEvents(streamName: string): Promise<esClient.ResolvedEvent[]> {
    const events: esClient.ResolvedEvent[] = [];

    return new Promise((resolve) => {
      this.esConnection.subscribeToStreamFrom(
        streamName,
        null,
        true,
        (_sub, event) => {
          events.push(event);
        },
        () => resolve(events),
      );
    });
  }

  public async down(): Promise<void> {}
}
